// File size: 160368 Bytes
// Author:    mtanaka
// Source:    rbn_a16.src
// Date:      00/00 00:00
// Binary:    section_000.bin

option authorName = "mtanaka/ffgriever";
option fileName = "rbn_a16-old_friends.src";
option spawnOrder = {-1, 1, -1, -1, -1, -1, -1, -1};
option positionFlags = 1;
option unknownFlags1 = 65535;
option unknownScale = {100, 0, 0};
option unknownPosition2d = {0, 0};

//======================================================================
//                Global and scratchpad variable imports                
//======================================================================
import global   short   シナリオフラグ = 0x0;
import global   u_char  g_btl_魔神竜 = 0xa18;
import global   u_char  g_btl_アントリオン = 0xa2f;
import global   u_char  g_btl_ノートリ_デスゲイズ = 0xa40;
import global   u_char  g_btl_nm_パック = 0xa43;
import global   u_char  g_btl_nm_テクスタ = 0xa44;
import global   u_char  g_btl_nm_花サボテン = 0xa45;
import global   u_char  g_btl_nm_レイス = 0xa46;
import global   u_char  g_btl_nm_ニーズヘッグ = 0xa47;
import global   u_char  g_btl_nm_ホワイトムース = 0xa48;
import global   u_char  g_btl_nm_リングドラゴン = 0xa49;
import global   u_char  g_btl_nm_ワイバーンロード = 0xa4a;
import global   u_char  g_btl_nm_マリリス = 0xa4b;
import global   u_char  g_btl_nm_エンケドラス = 0xa4c;
import global   u_char  g_btl_nm_ケロゲロス = 0xa4d;
import global   u_char  g_btl_nm_イシュタム = 0xa4e;
import global   u_char  g_btl_nm_チョッパー = 0xa4f;
import global   u_char  g_btl_nm_ボーパルバニー = 0xa50;
import global   u_char  g_btl_nm_マインドフレア = 0xa51;
import global   u_char  g_btl_nm_ブラッディ = 0xa52;
import global   u_char  g_btl_nm_アトモス = 0xa53;
import global   u_char  g_btl_nm_ロビー = 0xa54;
import global   u_char  g_btl_nm_ブライ = 0xa55;
import global   u_char  g_btl_nm_ダークスティール = 0xa56;
import global   u_char  g_btl_nm_ヴィラール = 0xa57;
import global   u_char  g_btl_nm_リンドヴルム = 0xa58;
import global   u_char  g_btl_nm_オーバーロード = 0xa59;
import global   u_char  g_btl_nm_ゴリアテ = 0xa5a;
import global   u_char  g_btl_nm_デスサイズ = 0xa5b;
import global   u_char  g_btl_nm_ディアボロス = 0xa5c;
import global   u_char  g_btl_nm_ピスコディーモン = 0xa5d;
import global   u_char  g_btl_nm_ワイルドモルボル = 0xa5e;
import global   u_char  g_btl_nm_カトブレパス = 0xa5f;
import global   u_char  g_btl_nm_ファーヴニル = 0xa60;
import global   u_char  g_btl_nm_パイルラスタ = 0xa61;
import global   u_char  quest_global_flag[94] = 0xb14;
import global   float   g_com_navi_footcalc[3] = 0xc7c;
import global   int     g_com_set_get_label = 0xc88;
import global   int     g_com_set_get_label_2 = 0xc8c;
import global   u_char  g_com_counter_for_horidasi[10] = 0xcd0;
import global   u_char  g_com_check_for_horidasi[2] = 0xcda;
import global   int     g_com_set_get_label_3 = 0xcf0;
import global   float   mp_map_set_angle = 0x900;
import global   float   mp_map_set_x = 0x904;
import global   float   mp_map_set_y = 0x908;
import global   float   mp_map_set_z = 0x90c;
import global   int     mp_map_flg = 0x910;
import global   char    mp_last_weather = 0x940;
import global   char    mp_4map = 0x941;
import global   char    mp_map_set_hitse = 0x943;
import global   u_char  g_iw_ヴィエラ進行 = 0x40e;
import global   u_char  g_iw_ヴィエラのお相手[1] = 0x40f;
import global   u_char  g_iw_sb03_rbn_flg[2] = 0x412;
import global   u_char  g_iw_クラン情報フラグ[3] = 0x41d;
import scratch1 short   scratch1_var_46 = 0x14;



script 振動処理(0)
{

	function init()
	{
		return;
	}


	function 大きな門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(8);
		vibsync();
		return;
	}


	function ＤＴ扉振動開始()
	{
		vibplay(3);
		vibsync();
		vibplay(0);
		vibsync();
		return;
	}


	function ＤＴ門振動開始()
	{
		vibplay(6);
		vibsync();
		vibplay(3);
		vibsync();
		vibplay(3);
		wait(20);
		vibstop();
		vibplay(7);
		wait(25);
		vibstop();
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_42;             // pos: 0x9d;


script setup(0)
{

	function init()
	{
		return;
	}


	function event()
	{
		switch (シナリオフラグ)
		{
			case lte(45):
				stopenvsoundall();
				setcharseplayall(0);
				break;
			case 0x190:
				file_var_42 = 1;
			default:
				break;
		}
		return;
	}
}

//======================================================================
//                         File scope variables                         
//======================================================================
u_char  file_var_41;             // pos: 0x9c;
u_char  file_var_43;             // pos: 0xc8;
u_char  file_var_44;             // pos: 0xc9;
u_char  file_var_45;             // pos: 0xca;


script 常駐監督(0)
{

	function init()
	{
		file_var_43 = 0;
		switch (シナリオフラグ)
		{
			case lte(45):
				fadeout(1);
				fadecancel_15a(2);
				sethpmenu(0);
				ucoff();
				settrapshowstatus(0);
				eventread(119);
				eventreadsync();
				sysReqi(1, 配置監督::ベース配置);
				sysReqwait(配置監督::ベース配置);
				sysReqi(1, トマジ::掲示板イベント時配置);
				sysReqwait(トマジ::掲示板イベント時配置);
				break;
			case 0x190:
				modelread(0x300000b);
				modelreadsync(0x300000b);
				file_var_42 = 1;
			default:
				break;
		}
		motionread(4);
		motionreadsync(4);
		puppetbind(6, 0x300000e, 1, 3);
		puppetsetpos(6, 59.4793243, 1.12499905, 38.3544006);
		puppetsetscale(6, 2.5);
		puppetbind(7, 0x300000e, 1, 5);
		puppetsetpos(7, 65.4820328, 0.799998999, 48.914547);
		puppetsetscale(7, 2);
		puppetbind(8, 0x300000e, 1, 2);
		puppetsetpos(8, 65.1796188, 0.799998999, 44.5354118);
		puppetsetscale(8, 2);
		puppetbind(9, 0x300000e, 1, 0);
		puppetsetpos(9, 64.7130432, 0.799998999, 43.5416183);
		puppetsetscale(9, 2);
		puppetbind(10, 0x300000e, 1, 4);
		puppetsetpos(10, 62.9179573, 0.799998999, 52.5902634);
		puppetsetscale(10, 2);
		puppetbind(11, 0x300000e, 1, 1);
		puppetsetpos(11, 56.8962212, 4.03499317, 48.3512421);
		puppetsetscale(11, 1.70000005);
		puppetbind(16, 0x300000f, 1, 0);
		puppetsetpos(16, 58.8186874, 1.07499897, 38.6293068);
		puppetdir(16, -1.53397298);
		puppetsetscale_47d(16, 2.4000001, 2.4000001, 2);
		puppetbind(17, 0x3000001, 3, 2);
		puppethide(17);
		puppetsetpos(17, 61.9612579, 0, 51.8409882);
		puppetdir(17, 0.601296008);
		puppetfetchambient(17, 61.3814125, 0, 50.2228775);
		puppetmotionloop(17, 1);
		puppetmotionstartframe(17, 0);
		puppetmotionloopframe(17, 0, -1);
		puppetmotionplay(17, 0x14000000);
		puppetrgbatrans(17, 1, 1, 1, 0, 0);
		puppetshow(17);
		puppetrgbatrans(17, 1, 1, 1, 1, 10);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		if (シナリオフラグ >= 0x1a4)
		{
			puppetbind(18, 0x3000000, 4, 1);
			puppethide(18);
			puppetsetpos(18, 52.6180305, 3.02499604, 46.1134109);
			puppetdir(18, -1.20069003);
			puppetfetchambient(18, 54.0841866, 3, 46.3128777);
			puppetmotionloop(18, 1);
			puppetmotionstartframe(18, 0);
			puppetmotionloopframe(18, 0, -1);
			puppetmotionplay(18, 0x14000001);
			puppetrgbatrans(18, 1, 1, 1, 0, 0);
			puppetshow(18);
			puppetrgbatrans(18, 1, 1, 1, 1, 10);
			rgbatrans(1, 1, 1, 0, 0);
			istouchucsync();
			clearhidecomplete();
			rgbatrans(1, 1, 1, 1, 10);
		}
		switch (シナリオフラグ)
		{
			case lt(0x15e):
				break;
			case lt(0x1a4):
				file_var_41 = 1;
				puppetbindoff(11);
				puppetbind_41d(0, 0x3000010);
				puppetsetpos(0, 55.7534561, 3.79999208, 50.0956116);
				puppetdir(0, 0.601297021);
				puppetsetscale(0, 0.800000012);
				puppetbind_41d(1, 0x300000e);
				puppetsetpos(1, 55.7534561, 3.79999208, 50.2956123);
				puppetbind_41d(2, 0x300000e);
				puppetsetpos(2, 55.5534554, 3.79999208, 49.8956108);
				puppetbind_41d(3, 0x300000e);
				puppetsetpos(3, 56.2899628, 3.79999208, 50.1303635);
				hidemapmodel(10);
				hidemapmodel(11);
				sysReqiall(1, reqArr0);
				motionread(3);
				motionreadsync(3);
				sysReqewi(1, ミュート::バルフレアバインド２);
				break;
		}
		return;
	}


	function main(1)
	{
		if (file_var_42 == 1)
		{
			sysReqi(1, 制御レクト::最速の飛空艇レクト配置);
			sysReqchg(3, 制御レクト::最速の飛空艇touch);
			sysReq(3, 読み込み監督０１::sakiyomi_read);
			sysReqwait(制御レクト::最速の飛空艇レクト配置);
		}
		switch (シナリオフラグ)
		{
			case lte(45):
				sysReqall(4, reqArr1);
				sysReqwaitall(reqArr1);
				sysReq(1, 常駐監督::EV掲示板とミュート);
				sysReqwait(常駐監督::EV掲示板とミュート);
				setquestorder(128, 1);
				シナリオフラグ = 65;
				setquestscenarioflag(0, 10);
				setquestscenarioflag(128, 30);
				g_btl_nm_パック = 1;
				break;
			default:
				sysReqi(1, 配置監督::ベース配置);
				sysReqi(2, トマジ::トマジ執政官着任前位置);
				break;
		}
		return;
	}


	function 駆け上がるマスター制御()
	{
		sysSysreqwait(NPC03::talk);
		sysReq(2, NPC03::駆け上がるマスター);
		return;
	}


	function 最速の飛空艇()
	{
		file_var_43 = 1;
		ucoff();
		fadeout(15);
		fadesync();
		sethpmenufast(0);
		settrapshowstatus(0);
		if (!(istownmap()))
		{
			setstatuserrordispdenystatus(1);
		}
		setcharseplayall(0);
		sysReqwaitall(reqArr2);
		sysReqwait(FRAN::フランバインド);
		sysReqwait(ミゲロ::ミゲロバインド);
		sysReqall(3, reqArr3);
		sysReqew(2, 読み込み監督０１::sakiyomi_play);
		sysReqew(1, 読み込み監督０１::sakiyomi_sync);
		シナリオフラグ = 0x1a4;
		addpartymember(3);
		addpartymember(2);
		showmapmodel(10);
		showmapmodel(11);
		puppetbindoff(0);
		puppetbindoff(1);
		puppetbindoff(2);
		puppetbindoff(3);
		file_var_44 = 1;
		wait(1);
		partyusemapid(1);
		setposparty(66.8888397, 0, 53.4143524, 1.07701802);
		resetbehindcamera(1.48643303, 0.800000012);
		sysReqall(4, reqArr4);
		sysReqall(1, reqArr5);
		showparty();
		wait(15);
		sysReq(1, ミュート::バルフレア進行確認バインド);
		sysReq(1, FRAN::消す);
		sysReq(1, ミゲロ::消す);
		sysReqchg(2, ミゲロ::ミゲロトーク);
		sysReqwait(ミュート::バルフレア進行確認バインド);
		sethpmenufast(0);
		wait(5);
		setstatuserrordispdenystatus(0);
		clearnavimapfootmark();
		setcharseplayall(1);
		fadein(30);
		ミュート.setkutipakustatus(1);
		ミュート.setunazukistatus(1);
		ミュート.amese(0, 0x100003f);
		ミュート.messync(0, 1);
		ミュート.setkutipakustatus(0);
		ミュート.setunazukistatus(0);
		sethpmenu(1);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_47;            // pos: 0x0;

	function EV掲示板とミュート()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysReq(2, NPC09::NPC09移動);
		NPC05.hide();
		NPC14.hide();
		eventplay_ec(0);
		wait(1);
		sysReqall(4, reqArr1);
		wait(60);
		eventsync();
		hideparty();
		motionread(1);
		motionreadsync(1);
		questeffectread(128);
		sysReq(1, ヴァン::掲示板イベント時配置);
		sysReqwait(トマジ::掲示板イベント時配置);
		sysReq(1, トマジ::掲示板イベント時表示);
		sysReqwait(ヴァン::掲示板イベント時配置);
		sethpmenufast(0);
		wait(15);
		camerastart_7e(0, 0x2000002);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(1);
		fadein(15);
		sysReqall(4, reqArr6);
		トマジ.stdmotionplay(0x1000016);
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		トマジ.amese(0, 0x1000095);
		トマジ.messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		effectreadsync();
		effectplay(0);
		sebsoundplay(0, 23);
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		askpos(0, 0, 127);
		local_var_47 = aaske(0, 0x1000096);
		mesclose(0);
		messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		effectplay_e2(0, 1);
		effectsync();
		トマジ.stdmotionplay(0x1000016);
		ヴァン.sysLookata(トマジ);
		if (local_var_47 == 0)
		{
			トマジ.setkutipakustatus(1);
			トマジ.setunazukistatus(1);
			トマジ.amese(0, 0x1000097);
			トマジ.messync(0, 1);
			トマジ.setkutipakustatus(0);
			トマジ.setunazukistatus(0);
		}
		else
		{
			トマジ.setkutipakustatus(1);
			トマジ.setunazukistatus(1);
			トマジ.amese(0, 0x1000098);
			トマジ.messync(0, 1);
			トマジ.setkutipakustatus(0);
			トマジ.setunazukistatus(0);
		}
		g_com_set_get_label = 0x8071;
		g_com_set_get_label_2 = 53;
		sysReqew(1, イベント特殊効果画面::ゲット＆メッセージ表示);
		sysReq(1, 台詞補助::トマジ台詞補助);
		sysReq(1, トマジ::ちょっと歩み寄る);
		sysReq(1, ヴァン::腕組み解く);
		wait(40);
		sysReq(1, 常駐カメラ::トマジカメラ２);
		sysReq(2, トマジ::立ち);
		sysReq(2, ヴァン::腕組み解く2);
		sysReqwait(台詞補助::トマジ台詞補助);
		sysReqwait(トマジ::立ち);
		wait(20);
		sebsoundplay(0, 39);
		additemmes(0x9000, 1);
		トマジ.stdmotionplay_2c2(0x1000016, 8);
		setmesmacro(0, 0, 1, 0x115b);
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		トマジ.amese(0, 0x1000099);
		トマジ.messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		g_com_set_get_label = 60;
		sysReqew(1, イベント特殊効果画面::システムメッセージ表示);
		トマジ.stdmotionplay(0x1000016);
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		トマジ.amese(0, 0x100009a);
		トマジ.messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		sysReq(1, トマジ::伝票あげる);
		wait(120);
		g_com_set_get_label = 0x8070;
		sysReqew(1, イベント特殊効果画面::だいじなものゲット);
		トマジ.stdmotionplay(0x1000002);
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		トマジ.amese(0, 0x100009b);
		トマジ.messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		questeffectread(192);
		effectreadsync();
		effectplay(0);
		effectsync();
		fadeout(15);
		fadesync();
		setcharseplayall(0);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		setstatuserrordispdenystatus(0);
		unkCall_5ac(0, 0, 0);
		sysReqall(4, reqArr1);
		sysReq(1, トマジ::トマジ執政官着任前位置);
		sysReqwait(トマジ::トマジ執政官着任前位置);
		sysReq(2, ヴァン::バインドオフ);
		sysReq(1, トマジ::トマジ執政官着任前挙動);
		partyusemapid(1);
		setposparty(67.7159119, 0, 45.7285118, -0.186379999);
		resetbehindcamera(-0.186379999, 0.800000012);
		cameraclear();
		showparty();
		motiondispose(1);
		wait(10);
		NPC05.show();
		NPC14.show();
		setalllightstatus(0, 1);
		setlightallcutstatus(1);
		sysReqall(4, reqArr6);
		resetbehindcamera(-0.186379999, 0.800000012);
		setnoupdatebehindcamera(1);
		showparty();
		if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
		{
			clearnavimapfootmark();
		}
		setnavimapfootmarkstatus(1);
		setcharseplayall(1);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		wait(1);
		fadein(15);
		sethpmenufast(0);
		settrapshowstatus(0);
		fadesync();
	localjmp_3:
		setmeswincaptionid(0, 3);
		setmesmacro(0, 0, 1, 0x115b);
		ames(0, 0x100009c, 0x3c0, 0x21c, 4);
		messync(0, 1);
		setmeswincaptionid(0, 3);
		ames(0, 0x100009d, 0x3c0, 0x1ff, 4);
		setmeswincaptionid(1, 0);
		askpos(1, 1, 1);
		file_var_45 = aask(1, 0x100009e, 0x3c0, 0x303, 4);
		mesclose(0);
		messync(0, 1);
		switch (file_var_45)
		{
			case 0:
				break;
			case 1:
				goto localjmp_3;
		}
		setnoupdatebehindcamera(0);
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function テクスタ撃破()
	{
		setquestscenarioflag(129, 70);
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(129);
		setquestorder(129, 0);
		setquestclear(129, 1);
		setquestok(129, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function キラートマト撃破()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(128);
		setquestorder(128, 0);
		setquestclear(128, 1);
		setquestok(128, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		setquestscenarioflag(128, 70);
		return;
	}


	function マリリスクリア()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		questresultwindow(136);
		setquestorder(136, 0);
		setquestclear(136, 1);
		setquestok(136, 0);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		setquestscenarioflag(136, 100);
		return;
	}
}


script 配置監督(0)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function ベース配置()
	{
		sysReq(1, hitactor01::setHitObj);
		sysReq(1, hitactor02::setHitObj);
		sysReq(1, hitactor03::setHitObj);
		sysReqchg(2, NPC01::talk_step01);
		sysReqchg(2, NPC02::talk_step01);
		sysReqchg(2, NPC03::talk_step01);
		sysReqchg(2, NPC04::talk_step01);
		sysReqchg(2, NPC05::talk_step01);
		sysReqchg(2, NPC06::talk_step01);
		sysReqchg(2, NPC07::talk_step01);
		sysReqchg(2, NPC08::talk_step01);
		sysReqchg(2, NPC09::talk_step01);
		sysReqchg(2, NPC10::talk_step01);
		sysReqchg(2, NPC11::talk_step01);
		sysReqchg(2, NPC12::talk_step01);
		sysReqchg(2, NPC13::talk_step01);
		motionread(0);
		motionreadsync(0);
		if ((シナリオフラグ >= 105 && !(isquestclear(129))))
		{
			sysReq(1, hitactor_cup::setHitObj);
			sysReqi(1, 依頼人::依頼人配置);
		}
		if (((シナリオフラグ >= 0x668 && g_iw_ヴィエラ進行 >= 2) && g_iw_ヴィエラ進行 < 5))
		{
			sysReqi(1, 放浪ヴィエラ::vie_set);
			sysReqwait(放浪ヴィエラ::vie_set);
			sysReq(1, 放浪ヴィエラ::vie_action);
		}
		switch (シナリオフラグ)
		{
			case lt(0x15e):
				sysReq(1, hitactor01_sol::setHitObj);
				sysReq(1, hitactor02_sol::setHitObj);
				sysReq(1, hitactor03_sol::setHitObj);
				break;
			case lt(0x1a4):
				sysReqi(1, FRAN::フランバインド);
				if (シナリオフラグ == 0x190)
				{
					sysReqi(1, ミゲロ::ミゲロバインド);
					sysReqwait(ミゲロ::ミゲロバインド);
				}
				switch (シナリオフラグ)
				{
					case lt(0x190):
						sysReqchg(2, ミュート::バルフレアtalk_通常時);
						ミュート.reqenable(2);
						sysReqchg(2, FRAN::talk_通常時);
						FRAN.reqenable(2);
						break;
					case 0x190:
						break;
					default:
						break;
				}
				break;
			default:
				sysReq(1, hitactor01_sol::setHitObj);
				sysReq(1, hitactor02_sol::setHitObj);
				sysReq(1, hitactor03_sol::setHitObj);
				break;
		}
		sysReqiall(5, reqArr2);
		return;
	}
}


script 常駐カメラ(0)
{

	function init()
	{
		return;
	}


	function トマジカメラ２()
	{
		camerastart_7e(0, 0x2000003);
		return;
	}
}


script 台詞補助(0)
{

	function init()
	{
		return;
	}


	function トマジ台詞補助()
	{
		トマジ.setkutipakustatus(1);
		トマジ.setunazukistatus(1);
		トマジ.amese(0, 0x100009f);
		トマジ.messync(0, 1);
		トマジ.setkutipakustatus(0);
		トマジ.setunazukistatus(0);
		return;
	}
}


script イベント特殊効果画面(0)
{

	function init()
	{
		return;
	}


	function だいじなものゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function システムメッセージ表示_FADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		setmeswinmesid(0, g_com_set_get_label);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout(15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function だいじなものゲット_MJFADE付()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(8);
		musictrans(-1, 30, 127);
		fadeout_d0(2, 15);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆メッセージ表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function ゲット＆ゲット()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_2, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_GET表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label_3, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}


	function GET_SYSMES_SYSMES表示()
	{
		musictrans(-1, 30, 32);
		setstatuserrorctrldenystatus_by_menu(1);
		seteventtimestop(1);
		moveblurcaptureonce();
		movebluraplpha(1);
		moveblurfade(15);
		moveblurfadesync();
		sebsoundplay(0, 40);
		additemmes(g_com_set_get_label, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_2);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		wait(6);
		setmeswinmesid(0, g_com_set_get_label_3);
		ames_1fe(0, 0x10000a0, 0x3c0, 0x21c, 4, 0);
		messync(0, 1);
		musictrans(-1, 30, 127);
		moveblurclearfade(15);
		moveblurfadesync();
		seteventtimestop(0);
		setstatuserrorctrldenystatus_by_menu(0);
		return;
	}
}


script 読み込み監督０１(0)
{

	function init()
	{
		if (file_var_42 == 1)
		{
			scratch1_var_46 = 0;
			eventread_e8(0, 109);
			eventreadsync_ea(0);
		}
		return;
	}


	function main(1)
	{
		return;
	}


	function sakiyomi_init()
	{
		scratch1_var_46 = 0;
		eventread_e8(0, 109);
		eventreadsync_ea(0);
		return;
	}


	function sakiyomi_read()
	{
		eventplay_ec(0);
		return;
	}


	function sakiyomi_play()
	{
		scratch1_var_46 = 1;
		return;
	}


	function sakiyomi_sync()
	{
		eventsync_ee(0);
		return;
	}
}


script Map_Director(0)
{

	function init()
	{
		resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
		return;
	}


	function main(1)
	{
		resetbehindcamera(getmapjumpanglebyindex(nowjumpindex()), -0.400000006);
		return;
	}
}


script 制御レクト(1)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function touch(3)
	{
		return;
	}


	function touchon(4)
	{
		return;
	}


	function touchoff(5)
	{
		return;
	}


	function レクトオフ()
	{
		rectdisable();
		return 0;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_48;            // pos: 0x0;

	function 最速の飛空艇レクト配置()
	{
		setrect_25(57.606144, 0.973760009, 57.245594, 2.0999999, 1.5, -1.50478303);
		local_var_48 = 0;
		if ((local_var_48 & 0x10000))
		{
			recttocircle();
		}
		if ((local_var_48 & 0x20000))
		{
			enable_by_chocobo(0);
		}
		else
		{
			enable_by_chocobo(1);
		}
		if ((local_var_48 & 0x40000))
		{
			enable_by_summon(0);
		}
		else
		{
			enable_by_summon(1);
		}
		if ((local_var_48 & 0x80000))
		{
			seteventwakerect(0);
		}
		else
		{
			seteventwakerect(1);
		}
		if ((local_var_48 & 0x100000))
		{
			settouchuconly(1);
		}
		else
		{
			settouchuconly(0);
		}
		local_var_48 = (local_var_48 & 0xffff);
		return;
	}


	function 最速の飛空艇touch()
	{
		ucoff_resetmotion(0);
		if (!(istownmap()))
		{
			suspendbattle();
		}
		rectdisable();
		switch (local_var_48)
		{
			case 6:
				sethpmenu(0);
				settrapshowstatus(0);
				break;
		}
		if (istownmap())
		{
			ucmove(54.4223862, 3, 56.7654915);
		}
		else
		{
			partystdmotionplay(0x100001d);
			if (check_move_battlestatus(0) == 1)
			{
				ucmove_52b(0, 54.4223862, 3, 56.7654915);
			}
			if (check_move_battlestatus(1) == 1)
			{
				ucmove_52b(1, 54.4223862, 3, 56.7654915);
			}
			if (check_move_battlestatus(2) == 1)
			{
				ucmove_52b(2, 54.4223862, 3, 56.7654915);
			}
			if (check_move_battlestatus(3) == 1)
			{
				ucmove_52b(3, 54.4223862, 3, 56.7654915);
			}
		}
		rectdisable();
		sysReq(1, 常駐監督::最速の飛空艇);
		sysReqwait(常駐監督::最速の飛空艇);
		return;
	}
}


script 掲示板(1)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		setrect_25(71.0544281, 2, 44.3617439, 1, 1, 0);
		setwh(1.5, 1.5, 0.800000012, 0.800000012);
		reqenable(2);
		recttocircle();
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		fieldsignicon(3);
		fieldsignmes(0x100004e);
		showfieldsign();
		settouchuconly(1);
		talkang(1.57079637);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_49[32];        // pos: 0x0;
	int     local_var_4a[32];        // pos: 0x80;
	int     local_var_4b[32];        // pos: 0x100;
	u_char  local_var_4c[32];        // pos: 0x180;
	u_char  local_var_4d;            // pos: 0x1a0;
	char    local_var_4e;            // pos: 0x1a1;
	u_char  local_var_4f;            // pos: 0x1a2;
	u_char  local_var_50;            // pos: 0x1a3;
	u_char  local_var_51;            // pos: 0x1a4;
	int     local_var_52;            // pos: 0x1a8;
	short   local_var_54;            // pos: 0x1b0;
	short   local_var_55;            // pos: 0x1b2;
	u_char  local_var_56;            // pos: 0x1b4;
	u_char  local_var_57;            // pos: 0x1b5;
	u_char  local_var_58;            // pos: 0x1b6;
	u_char  local_var_59;            // pos: 0x1b7;
	u_char  local_var_5a[33];        // pos: 0x1b8;
	u_char  local_var_5b;            // pos: 0x1d9;
	u_char  local_var_5c;            // pos: 0x1e1;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		if (シナリオフラグ > 80)
		{
			if (g_com_counter_for_horidasi[4] >= 255)
			{
				g_com_counter_for_horidasi[4] = 255;
			}
			else
			{
				g_com_counter_for_horidasi[4] = (g_com_counter_for_horidasi[4] + 1);
			}
			if ((g_com_counter_for_horidasi[4] >= 20 && !((g_com_check_for_horidasi[0] & 16))))
			{
				sethoridashi(0xd07d);
				g_com_check_for_horidasi[0] = (g_com_check_for_horidasi[0] | 16);
			}
			if ((g_com_counter_for_horidasi[4] >= 40 && !((g_com_check_for_horidasi[0] & 32))))
			{
				sethoridashi(0xd065);
				g_com_check_for_horidasi[0] = (g_com_check_for_horidasi[0] | 32);
			}
			local_var_4d = 0;
			local_var_4f = 0;
			local_var_4e = -1;
			local_var_49[0] = g_btl_nm_テクスタ;
			local_var_49[1] = g_btl_nm_花サボテン;
			local_var_49[2] = g_btl_nm_レイス;
			local_var_49[3] = g_btl_nm_ニーズヘッグ;
			local_var_49[4] = g_btl_nm_ホワイトムース;
			local_var_49[5] = g_btl_nm_リングドラゴン;
			local_var_49[6] = g_btl_nm_ワイバーンロード;
			local_var_49[7] = g_btl_nm_マリリス;
			local_var_49[8] = g_btl_nm_エンケドラス;
			local_var_49[9] = g_btl_nm_ケロゲロス;
			local_var_49[10] = g_btl_nm_イシュタム;
			local_var_49[11] = g_btl_nm_チョッパー;
			local_var_49[12] = g_btl_nm_ボーパルバニー;
			local_var_49[13] = g_btl_nm_マインドフレア;
			local_var_49[14] = g_btl_nm_ブラッディ;
			local_var_49[15] = g_btl_nm_アトモス;
			local_var_49[16] = g_btl_nm_ロビー;
			local_var_49[17] = g_btl_nm_ブライ;
			local_var_49[18] = g_btl_nm_ダークスティール;
			local_var_49[19] = g_btl_nm_ヴィラール;
			local_var_49[20] = g_btl_nm_リンドヴルム;
			local_var_49[21] = g_btl_nm_オーバーロード;
			local_var_49[22] = g_btl_nm_ゴリアテ;
			local_var_49[23] = g_btl_nm_デスサイズ;
			local_var_49[24] = g_btl_ノートリ_デスゲイズ;
			local_var_49[25] = g_btl_nm_ディアボロス;
			local_var_49[26] = g_btl_nm_ピスコディーモン;
			local_var_49[27] = g_btl_nm_ワイルドモルボル;
			local_var_49[28] = g_btl_nm_カトブレパス;
			local_var_49[29] = g_btl_nm_ファーヴニル;
			local_var_49[30] = g_btl_nm_パイルラスタ;
			local_var_4a[0] = 1;
			local_var_4a[1] = 2;
			local_var_4a[2] = 2;
			local_var_4a[3] = 2;
			local_var_4a[4] = 4;
			local_var_4a[5] = 4;
			local_var_4a[6] = 3;
			local_var_4a[7] = 4;
			local_var_4a[8] = 3;
			local_var_4a[9] = 3;
			local_var_4a[10] = 3;
			local_var_4a[11] = 4;
			local_var_4a[12] = 4;
			local_var_4a[13] = 5;
			local_var_4a[14] = 6;
			local_var_4a[15] = 4;
			local_var_4a[16] = 7;
			local_var_4a[17] = 5;
			local_var_4a[18] = 5;
			local_var_4a[19] = 6;
			local_var_4a[20] = 6;
			local_var_4a[21] = 7;
			local_var_4a[22] = 7;
			local_var_4a[23] = 7;
			local_var_4a[24] = 8;
			local_var_4a[25] = 7;
			local_var_4a[26] = 6;
			local_var_4a[27] = 8;
			local_var_4a[28] = 7;
			local_var_4a[29] = 8;
			local_var_4a[30] = 8;
			local_var_4b[0] = 123;
			local_var_4b[1] = 0x115;
			local_var_4b[2] = 0x26d;
			local_var_4b[3] = 0x18e;
			local_var_4b[4] = 0x2e8;
			local_var_4b[5] = 0x247;
			local_var_4b[6] = 16;
			local_var_4b[7] = 75;
			local_var_4b[8] = 0x1a2;
			local_var_4b[9] = 233;
			local_var_4b[10] = 0x19d;
			local_var_4b[11] = 0x1da;
			local_var_4b[12] = 0x200;
			local_var_4b[13] = 0x1a4;
			local_var_4b[14] = 0x105;
			local_var_4b[15] = 0x2d6;
			local_var_4b[16] = 0x2b3;
			local_var_4b[17] = 0x2f2;
			local_var_4b[18] = 0x3b9;
			local_var_4b[19] = 0x341;
			local_var_4b[20] = 0x3c5;
			local_var_4b[21] = 0x3a5;
			local_var_4b[22] = 0x2b2;
			local_var_4b[23] = 0x2b4;
			local_var_4b[24] = 0x2da;
			local_var_4b[25] = 0x191;
			local_var_4b[26] = 0x1de;
			local_var_4b[27] = 0x202;
			local_var_4b[28] = 0x1a0;
			local_var_4b[29] = 0x1dc;
			local_var_4b[30] = 0x2e4;
			local_var_52 = getclanrank();
			goto localjmp_20;
		localjmp_4:
			goto localjmp_19;
		localjmp_5:
			シナリオフラグ = 0x122;
			goto localjmp_19;
		localjmp_6:
			シナリオフラグ = 0x15e;
			goto localjmp_19;
		localjmp_7:
			シナリオフラグ = 0x29e;
			goto localjmp_19;
		localjmp_8:
			シナリオフラグ = 0x4ba;
			goto localjmp_19;
		localjmp_9:
			シナリオフラグ = 0x604;
			goto localjmp_19;
		localjmp_10:
			シナリオフラグ = 0x79e;
			goto localjmp_19;
		localjmp_11:
			シナリオフラグ = 0x7be;
			goto localjmp_19;
		localjmp_12:
			シナリオフラグ = 0xbb8;
			goto localjmp_19;
		localjmp_13:
			シナリオフラグ = 0xcb2;
			goto localjmp_19;
		localjmp_14:
			シナリオフラグ = 0x1004;
			goto localjmp_19;
		localjmp_15:
			シナリオフラグ = 0x141e;
			goto localjmp_19;
		localjmp_16:
			シナリオフラグ = 0x17d4;
			goto localjmp_19;
		localjmp_17:
			シナリオフラグ = 0x2af8;
			goto localjmp_19;
			if (regY == 0) goto localjmp_4;
			if (regY <= 1) goto localjmp_5;
			if (regY <= 2) goto localjmp_6;
			if (regY <= 3) goto localjmp_7;
			if (regY <= 4) goto localjmp_8;
			if (regY <= 5) goto localjmp_9;
			if (regY <= 6) goto localjmp_10;
			if (regY <= 7) goto localjmp_11;
			if (regY <= 8) goto localjmp_12;
			if (regY <= 9) goto localjmp_13;
			if (regY <= 10) goto localjmp_14;
			if (regY <= 11) goto localjmp_15;
			if (regY <= 12) goto localjmp_16;
			goto localjmp_17;
		localjmp_19:
			goto localjmp_21;
		localjmp_20:
			local_var_52 = getclanrank();
			local_var_54 = handbook_getkillcount(64);
			local_var_55 = handbook_getordernum();
		localjmp_21:
			sysDVAR(1, local_var_55);
			sysDVAR(2, local_var_54);
			switch (シナリオフラグ)
			{
				case lt(105):
					local_var_51 = 0;
					break;
				case lt(0x122):
					local_var_51 = 2;
					break;
				case lt(0x15e):
					local_var_51 = 2;
					break;
				case lt(0x29e):
					local_var_51 = 3;
					break;
				case lt(0x4ba):
					local_var_51 = 4;
					break;
				case lt(0x604):
					local_var_51 = 8;
					break;
				case lt(0x79e):
					local_var_51 = 10;
					break;
				case lt(0x7be):
					local_var_51 = 11;
					break;
				case lt(0xbb8):
					local_var_51 = 12;
					break;
				case lt(0xcb2):
					local_var_51 = 18;
					break;
				case lt(0x1004):
					local_var_51 = 19;
					break;
				case lt(0x141e):
					local_var_51 = 24;
					break;
				case lt(0x17d4):
					local_var_51 = 30;
					break;
				default:
					local_var_51 = 31;
					break;
			}
			local_var_56 = 0;
			sysDVAR(10, local_var_56);
			local_var_58 = 0;
			while (true)
			{
				local_var_5b = 0;
				for (local_var_59 = 0; local_var_59 <= (local_var_51 - 1); local_var_59 = (local_var_59 + 1))
				{
					local_var_5a[local_var_59] = 0;
				}
				sysDVAR(7, local_var_59);
				for (local_var_59 = 0; local_var_59 <= (local_var_51 - 1); local_var_59 = (local_var_59 + 1))
				{
					if (isquestclear((local_var_59 + 129)))
					{
						local_var_5a[local_var_59] = 1;
					}
					else if (!(isquestorder((local_var_59 + 129))))
					{
						if (local_var_52 >= local_var_4a[local_var_59])
						{
							switch ((local_var_59 + 129))
							{
								case 168:
									if (g_btl_nm_ファーヴニル == 2)
									{
										local_var_5c = 1;
									}
									else
									{
										local_var_5c = 0;
									}
									break;
								case 167:
									if (g_btl_アントリオン == 2)
									{
										local_var_5c = 1;
									}
									else
									{
										local_var_5c = 0;
									}
									break;
								case 171:
									if ((quest_global_flag[7] >= 7 && g_btl_魔神竜 == 2))
									{
										local_var_5c = 1;
									}
									else
									{
										local_var_5c = 0;
									}
									break;
								default:
									local_var_5c = 1;
									break;
							}
							if (local_var_5c == 1)
							{
								local_var_4c[local_var_59] = local_var_5b;
								local_var_5b = (local_var_5b + 1);
								local_var_5a[local_var_59] = 2;
								if (local_var_4f == 0)
								{
									local_var_4e = local_var_4c[local_var_59];
									local_var_50 = (local_var_59 + 129);
									local_var_4f = 1;
								}
							}
							else
							{
								local_var_5a[local_var_59] = 0;
							}
						}
						else
						{
							local_var_5a[local_var_59] = 0;
						}
					}
					else if (local_var_49[local_var_59] == 2)
					{
						local_var_4c[local_var_59] = local_var_5b;
						local_var_5b = (local_var_5b + 1);
						local_var_5a[local_var_59] = 4;
					}
					else
					{
						local_var_4c[local_var_59] = local_var_5b;
						local_var_5b = (local_var_5b + 1);
						local_var_5a[local_var_59] = 3;
					}
				}
				if (local_var_5b == 0)
				{
					if (local_var_58 == 0)
					{
						if (local_var_59 >= 31)
						{
							setmeswincaptionid(0, 3);
							amese(0, 0x1000051);
							messync(0, 1);
						}
						else
						{
							setmeswincaptionid(0, 3);
							amese(0, 0x1000052);
							messync(0, 1);
						}
						break;
					}
					break;
				}
				else
				{
					if ((local_var_5b + 1) >= 7)
					{
						local_var_5b = 6;
					}
					setmeswinline(0, (local_var_5b + 1));
					for (local_var_59 = 0; local_var_59 <= 30; local_var_59 = (local_var_59 + 1))
					{
						if (local_var_59 <= (local_var_51 - 1))
						{
							if (local_var_5a[local_var_59] >= 2)
							{
								switch (local_var_5a[local_var_59])
								{
									case 2:
										setmesmacro(0, local_var_59, 1, 0x806d);
										break;
									case 3:
										setmesmacro(0, local_var_59, 1, 0x806e);
										break;
									case 4:
										setmesmacro(0, local_var_59, 1, 0x806f);
										break;
								}
							}
							else
							{
								setaskselectignore(0, local_var_59);
							}
						}
						else
						{
							setaskselectignore(0, local_var_59);
						}
					}
					if ((local_var_4d == 0 && local_var_4e > 0))
					{
						askpos(0, local_var_4e, local_var_59);
						local_var_4d = 1;
					}
					else
					{
						askpos(0, local_var_4c[local_var_56], local_var_59);
					}
					local_var_56 = aask(0, 0x1000053, 120, 0x3fe, 1);
					mesclose(0);
					messync(0, 1);
					if (local_var_56 == 31)
					{
						break;
					}
					questeffectread(((local_var_56 + 129) + 0));
					effectreadsync();
					effectplay(0);
					sebsoundplay(0, 23);
					if ((local_var_56 + 129) >= 168)
					{
						setmesmacro(0, 0, 0, (local_var_56 + 2));
					}
					else
					{
						setmesmacro(0, 0, 0, (local_var_56 + 1));
					}
					switch (local_var_5a[local_var_56])
					{
						case 2:
							setmesmacro(0, 1, 1, 0x806d);
							break;
						case 3:
							setmesmacro(0, 1, 1, 0x806e);
							break;
						case 4:
							setmesmacro(0, 1, 1, 0x806f);
							break;
					}
					if (local_var_5a[local_var_56] == 2)
					{
						steppoint(0);
						switch (local_var_56)
						{
							case 0:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000054);
								mesclose(0);
								messync(0, 1);
								break;
							case 1:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000055);
								mesclose(0);
								messync(0, 1);
								break;
							case 2:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000056);
								mesclose(0);
								messync(0, 1);
								break;
							case 3:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000057);
								mesclose(0);
								messync(0, 1);
								break;
							case 4:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000058);
								mesclose(0);
								messync(0, 1);
								break;
							case 5:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000059);
								mesclose(0);
								messync(0, 1);
								break;
							case 6:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005a);
								mesclose(0);
								messync(0, 1);
								break;
							case 7:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005b);
								mesclose(0);
								messync(0, 1);
								break;
							case 8:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005c);
								mesclose(0);
								messync(0, 1);
								break;
							case 9:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005d);
								mesclose(0);
								messync(0, 1);
								break;
							case 10:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005e);
								mesclose(0);
								messync(0, 1);
								break;
							case 11:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100005f);
								mesclose(0);
								messync(0, 1);
								break;
							case 12:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000060);
								mesclose(0);
								messync(0, 1);
								break;
							case 13:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000061);
								mesclose(0);
								messync(0, 1);
								break;
							case 14:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000062);
								mesclose(0);
								messync(0, 1);
								break;
							case 15:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000063);
								mesclose(0);
								messync(0, 1);
								break;
							case 16:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000064);
								mesclose(0);
								messync(0, 1);
								break;
							case 17:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000065);
								mesclose(0);
								messync(0, 1);
								break;
							case 18:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000066);
								mesclose(0);
								messync(0, 1);
								break;
							case 19:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000067);
								mesclose(0);
								messync(0, 1);
								break;
							case 20:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000068);
								mesclose(0);
								messync(0, 1);
								break;
							case 21:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000069);
								mesclose(0);
								messync(0, 1);
								break;
							case 22:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006a);
								mesclose(0);
								messync(0, 1);
								break;
							case 23:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006b);
								mesclose(0);
								messync(0, 1);
								break;
							case 24:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006c);
								mesclose(0);
								messync(0, 1);
								break;
							case 25:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006d);
								mesclose(0);
								messync(0, 1);
								break;
							case 26:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006e);
								mesclose(0);
								messync(0, 1);
								break;
							case 27:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100006f);
								mesclose(0);
								messync(0, 1);
								break;
							case 28:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000070);
								mesclose(0);
								messync(0, 1);
								break;
							case 29:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000071);
								mesclose(0);
								messync(0, 1);
								break;
							case 30:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000072);
								mesclose(0);
								messync(0, 1);
								break;
						}
					}
					else
					{
						steppoint(1);
						switch (local_var_56)
						{
							case 0:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000073);
								mesclose(0);
								messync(0, 1);
								break;
							case 1:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000074);
								mesclose(0);
								messync(0, 1);
								break;
							case 2:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000075);
								mesclose(0);
								messync(0, 1);
								break;
							case 3:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000076);
								mesclose(0);
								messync(0, 1);
								break;
							case 4:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000077);
								mesclose(0);
								messync(0, 1);
								break;
							case 5:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000078);
								mesclose(0);
								messync(0, 1);
								break;
							case 6:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000079);
								mesclose(0);
								messync(0, 1);
								break;
							case 7:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007a);
								mesclose(0);
								messync(0, 1);
								break;
							case 8:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007b);
								mesclose(0);
								messync(0, 1);
								break;
							case 9:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007c);
								mesclose(0);
								messync(0, 1);
								break;
							case 10:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007d);
								mesclose(0);
								messync(0, 1);
								break;
							case 11:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007e);
								mesclose(0);
								messync(0, 1);
								break;
							case 12:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100007f);
								mesclose(0);
								messync(0, 1);
								break;
							case 13:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000080);
								mesclose(0);
								messync(0, 1);
								break;
							case 14:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000081);
								mesclose(0);
								messync(0, 1);
								break;
							case 15:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000082);
								mesclose(0);
								messync(0, 1);
								break;
							case 16:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000083);
								mesclose(0);
								messync(0, 1);
								break;
							case 17:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000084);
								mesclose(0);
								messync(0, 1);
								break;
							case 18:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000085);
								mesclose(0);
								messync(0, 1);
								break;
							case 19:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000086);
								mesclose(0);
								messync(0, 1);
								break;
							case 20:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000087);
								mesclose(0);
								messync(0, 1);
								break;
							case 21:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000088);
								mesclose(0);
								messync(0, 1);
								break;
							case 22:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000089);
								mesclose(0);
								messync(0, 1);
								break;
							case 23:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008a);
								mesclose(0);
								messync(0, 1);
								break;
							case 24:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008b);
								mesclose(0);
								messync(0, 1);
								break;
							case 25:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008c);
								mesclose(0);
								messync(0, 1);
								break;
							case 26:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008d);
								mesclose(0);
								messync(0, 1);
								break;
							case 27:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008e);
								mesclose(0);
								messync(0, 1);
								break;
							case 28:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x100008f);
								mesclose(0);
								messync(0, 1);
								break;
							case 29:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000090);
								mesclose(0);
								messync(0, 1);
								break;
							case 30:
								setmeswincaptionid(0, 3);
								askpos(0, 0, 1);
								local_var_57 = aaske(0, 0x1000091);
								mesclose(0);
								messync(0, 1);
								break;
						}
					}
					steppoint(2);
					effectplay_e2(0, 1);
					if (local_var_57 == 0)
					{
						if (local_var_5a[local_var_56] == 2)
						{
							wait(15);
							settalknpcname(local_var_4b[local_var_56]);
							setmesmacro(0, 0, 0, (local_var_56 + 1));
							setmeswincaptionid(0, 3);
							amese(0, 0x1000092);
							messync(0, 1);
							setquestorder((local_var_56 + 129), 1);
							setquestscenarioflag((local_var_56 + 129), 10);
						}
						effectsync_e3(0);
						openfullscreenmenu_48d(4, (local_var_56 + 129));
						local_var_58 = 1;
						if (local_var_5a[local_var_56] == 2)
						{
							break;
						}
						wait(30);
					}
					else
					{
						local_var_58 = 1;
						wait(20);
					}
					effectcancel();
				}
			}
			effectcancel();
		}
		else
		{
			setmesmacro(0, 0, 1, 0x806e);
			askpos(0, 0, 2);
			local_var_56 = aask(0, 0x1000093, 120, 0x3fe, 1);
			if (local_var_56 == 0)
			{
				questeffectread(128);
				effectreadsync();
				effectplay(0);
				sebsoundplay(0, 23);
				setmesmacro(0, 0, 1, 0x806e);
				setmeswincaptionid(0, 3);
				amese(0, 0x1000094);
				messync(0, 1);
				effectplay_e2(0, 1);
				effectsync();
			}
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function fieldsign1(8)
	{
		return;
	}


	function fieldsign2(13)
	{
		return;
	}
}


script door_out(1)
{

	function init()
	{
		fieldsign(0);
		reqenable(2);
		return;
	}


	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		mp_map_set_angle = 1.57079637;
		pausesestop();
		sysReqew(1, map_ダミーＰＣ::扉オープン１);
		mp_map_flg = 0;
		mp_map_flg = sebsoundplay(1, 1);
		animeplay(0);
		wait(15);
		regI3 = sysucoff();
		if (regI3)
		{
			clearmapjumpstatus();
			sysucon();
		}
		else
		{
			mp_last_weather = getweatherslot();
			steppoint(3);
			mp_4map = 30;
			fadelayer(6);
			fadeprior(255);
			fadeout_d0(2, mp_4map);
			steppoint(4);
			wait(15);
			steppoint(3);
			spotsoundtrans(15, 0);
			steppoint(2);
			wait(17);
			stopspotsound();
			pausesestop();
			hideparty();
			eventsoundplaysync(mp_map_flg);
			steppoint(1);
			setbattlethinkstatus_freetarget_group(-1, 0);
			voicestopall();
			mapjump(0x123, 14, 1);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	char    local_var_5d;            // pos: 0x2;

	function フィールドサインＯＫ()
	{
		fieldsignicon(2);
		reqenable(2);
		local_var_5d = 0;
		return;
	}


	function フィールドサインＮＯＴ()
	{
		fieldsignicon(3);
		reqenable(2);
		local_var_5d = 1;
		return;
	}


	function フィールドサインＯＮ()
	{
		reqdisable(3);
		reqenable(8);
		reqenable(13);
		reqenable(2);
		return;
	}


	function フィールドサインＯＦＦ()
	{
		reqdisable(8);
		reqdisable(13);
		reqdisable(2);
		return;
	}
}

symlink NPC02(NPC01) : 0x1;

symlink NPC03(NPC01) : 0x2;

symlink NPC04(NPC01) : 0x3;

symlink NPC05(NPC01) : 0x4;

symlink NPC06(NPC01) : 0x5;

symlink NPC07(NPC01) : 0x6;

symlink NPC08(NPC01) : 0x7;

symlink NPC09(NPC01) : 0x8;

symlink NPC10(NPC01) : 0x9;

symlink NPC11(NPC01) : 0xa;

symlink NPC12(NPC01) : 0xb;

symlink NPC13(NPC01) : 0xc;

symlink NPC14(NPC01) : 0xd;

symlink NPC15(NPC01) : 0xe;

symlink NPC16(NPC01) : 0xf;

symlink NPC17(NPC01) : 0x10;


script NPC01(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_5e;            // pos: 0x0;

	function init()
	{
		local_var_5e = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_60;            // pos: 0xd;
	float   local_var_61[15];        // pos: 0x10;
	float   local_var_62[5];         // pos: 0x4c;
	float   local_var_64;            // pos: 0x64;
	float   local_var_65[4];         // pos: 0x68;
	float   local_var_68;            // pos: 0x80;
	int     local_var_6b;            // pos: 0x9c;
	u_char  local_var_6d;            // pos: 0xa4;
	u_char  local_var_6f;            // pos: 0xa6;
	u_char  local_var_70;            // pos: 0xa7;
	u_char  local_var_71;            // pos: 0xa8;

	function NPC配置()
	{
		hidecomplete();
		switch (local_var_5e)
		{
			case 0:
				return;
			case 1:
				local_var_60 = 2;
				local_var_70 = 5;
				local_var_71 = 1;
				local_var_6d = 1;
				local_var_6f = 2;
				local_var_6b = 80;
				local_var_68 = 0;
				local_var_61[0] = 58.1851501;
				local_var_61[1] = 0.200000003;
				local_var_61[2] = 47.4090157;
				local_var_61[3] = 58.2539864;
				local_var_61[4] = 0.200000003;
				local_var_61[5] = 42.7403603;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 0.000877999992;
				local_var_62[1] = 1.54562902;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 58.1851501;
				local_var_65[1] = 0.200000003;
				local_var_65[2] = 47.4090157;
				local_var_65[3] = 0.000877999992;
				break;
			case 2:
				local_var_60 = 1;
				local_var_70 = 5;
				local_var_71 = 2;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 58.5069237;
				local_var_61[1] = 0.25;
				local_var_61[2] = 38.6169167;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 1.41224802;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 58.5069237;
				local_var_65[1] = 0.25;
				local_var_65[2] = 38.6169167;
				local_var_65[3] = 1.41224802;
				break;
			case 3:
				local_var_60 = 2;
				local_var_70 = 2;
				local_var_71 = 2;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 60.2575111;
				local_var_61[1] = 0;
				local_var_61[2] = 36.7601776;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -1.14733601;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 60.2575111;
				local_var_65[1] = 0;
				local_var_65[2] = 36.7601776;
				local_var_65[3] = -1.14733601;
				break;
			case 4:
				if (シナリオフラグ >= 0x1a4)
				{
					local_var_60 = 7;
					local_var_70 = 1;
					local_var_71 = 4;
					local_var_6d = 0;
					local_var_6f = 0;
					local_var_6b = 0;
					local_var_68 = 0;
					local_var_61[0] = 56.2000008;
					local_var_61[1] = 3;
					local_var_61[2] = 47.9358482;
					local_var_61[3] = 0;
					local_var_61[4] = 0;
					local_var_61[5] = 0;
					local_var_61[6] = 0;
					local_var_61[7] = 0;
					local_var_61[8] = 0;
					local_var_61[9] = 0;
					local_var_61[10] = 0;
					local_var_61[11] = 0;
					local_var_62[0] = 2.33711004;
					local_var_62[1] = 0;
					local_var_62[2] = 0;
					local_var_62[3] = 0;
					local_var_65[0] = 56.2000008;
					local_var_65[1] = 3;
					local_var_65[2] = 47.9358482;
					local_var_65[3] = 2.33711004;
				}
				else
				{
					local_var_60 = 0;
					local_var_70 = 1;
					local_var_71 = 3;
					local_var_6d = 0;
					local_var_6f = 0;
					local_var_6b = 0;
					local_var_68 = 0;
					local_var_61[0] = 56.2000008;
					local_var_61[1] = 3;
					local_var_61[2] = 47.9358482;
					local_var_61[3] = 0;
					local_var_61[4] = 0;
					local_var_61[5] = 0;
					local_var_61[6] = 0;
					local_var_61[7] = 0;
					local_var_61[8] = 0;
					local_var_61[9] = 0;
					local_var_61[10] = 0;
					local_var_61[11] = 0;
					local_var_62[0] = 2.33711004;
					local_var_62[1] = 0;
					local_var_62[2] = 0;
					local_var_62[3] = 0;
					local_var_65[0] = 56.2000008;
					local_var_65[1] = 3;
					local_var_65[2] = 47.9358482;
					local_var_65[3] = 2.33711004;
				}
				break;
			case 5:
				local_var_60 = 2;
				local_var_70 = 3;
				local_var_71 = 2;
				local_var_6d = 1;
				local_var_6f = 3;
				local_var_6b = 50;
				local_var_68 = 0;
				local_var_61[0] = 52.8109589;
				local_var_61[1] = 3;
				local_var_61[2] = 50.0887032;
				local_var_61[3] = 54.4354286;
				local_var_61[4] = 3;
				local_var_61[5] = 53.1337128;
				local_var_61[6] = 53.0522537;
				local_var_61[7] = 3;
				local_var_61[8] = 54.5154991;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 2.38620996;
				local_var_62[1] = 2.60082197;
				local_var_62[2] = 2.66825008;
				local_var_62[3] = 0;
				local_var_65[0] = 52.8109589;
				local_var_65[1] = 3;
				local_var_65[2] = 50.0887032;
				local_var_65[3] = 2.38620996;
				break;
			case 6:
				local_var_60 = 1;
				local_var_70 = 2;
				local_var_71 = 1;
				local_var_6d = 1;
				local_var_6f = 0;
				local_var_6b = 50;
				local_var_68 = 0;
				local_var_61[0] = 58.4990196;
				local_var_61[1] = 0;
				local_var_61[2] = 52.5934219;
				local_var_61[3] = 60.5672188;
				local_var_61[4] = 0;
				local_var_61[5] = 56.2358322;
				local_var_61[6] = 54.1136093;
				local_var_61[7] = 3;
				local_var_61[8] = 56.4476814;
				local_var_61[9] = 60.3421936;
				local_var_61[10] = -0;
				local_var_61[11] = 55.9532471;
				local_var_62[0] = 1.55649805;
				local_var_62[1] = -0.457307011;
				local_var_62[2] = -2.75579691;
				local_var_62[3] = -2.39043999;
				local_var_65[0] = 58.4990196;
				local_var_65[1] = 0;
				local_var_65[2] = 52.5934219;
				local_var_65[3] = 1.55649805;
				break;
			case 7:
				local_var_60 = 8;
				local_var_70 = 2;
				local_var_71 = 0;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 60.5939178;
				local_var_61[1] = 0;
				local_var_61[2] = 38.2134018;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -1.72094297;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 60.5939178;
				local_var_65[1] = 0;
				local_var_65[2] = 38.2134018;
				local_var_65[3] = -1.72094297;
				break;
			case 8:
				local_var_60 = 2;
				local_var_70 = 4;
				local_var_71 = 0;
				local_var_6d = 1;
				local_var_6f = 0;
				local_var_6b = 30;
				local_var_68 = 0;
				local_var_61[0] = 69.4158249;
				local_var_61[1] = 0;
				local_var_61[2] = 41.9358253;
				local_var_61[3] = 68.2814407;
				local_var_61[4] = 0;
				local_var_61[5] = 43.213131;
				local_var_61[6] = 68.0261383;
				local_var_61[7] = 0;
				local_var_61[8] = 46.0941048;
				local_var_61[9] = 68.2814407;
				local_var_61[10] = 0;
				local_var_61[11] = 43.213131;
				local_var_62[0] = -1.12457895;
				local_var_62[1] = -1.23331106;
				local_var_62[2] = -1.05569398;
				local_var_62[3] = -1.23331106;
				local_var_65[0] = 69.4158249;
				local_var_65[1] = 0;
				local_var_65[2] = 41.9358253;
				local_var_65[3] = -1.12457895;
				break;
			case 9:
				local_var_60 = 1;
				local_var_70 = 4;
				local_var_71 = 1;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 65.5987244;
				local_var_61[1] = 0;
				local_var_61[2] = 44.5263596;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -2.23550105;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 65.5987244;
				local_var_65[1] = 0;
				local_var_65[2] = 44.5263596;
				local_var_65[3] = -2.23550105;
				break;
			case 10:
				local_var_60 = 2;
				local_var_70 = 1;
				local_var_71 = 1;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 64.819664;
				local_var_61[1] = 0;
				local_var_61[2] = 43.688961;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 0.995293021;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 64.819664;
				local_var_65[1] = 0;
				local_var_65[2] = 43.688961;
				local_var_65[3] = 0.995293021;
				break;
			case 11:
				local_var_60 = 1;
				local_var_70 = 4;
				local_var_71 = 0;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 63.1285934;
				local_var_61[1] = 0;
				local_var_61[2] = 53.0136681;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -2.24278402;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 63.1285934;
				local_var_65[1] = 0;
				local_var_65[2] = 53.0136681;
				local_var_65[3] = -2.24278402;
				break;
			case 12:
				local_var_60 = 7;
				local_var_70 = 3;
				local_var_71 = 2;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 60.3046455;
				local_var_61[1] = 0.0250000004;
				local_var_61[2] = 46.3775482;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -2.22091007;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 60.3046455;
				local_var_65[1] = 0.0250000004;
				local_var_65[2] = 46.3775482;
				local_var_65[3] = -2.22091007;
				break;
			case 13:
				if (シナリオフラグ >= 0x1a4)
				{
					local_var_60 = 1;
					local_var_70 = 2;
					local_var_71 = 2;
					local_var_6d = 0;
					local_var_6f = 0;
					local_var_6b = 0;
					local_var_68 = 0;
					local_var_61[0] = 56.2656898;
					local_var_61[1] = 3;
					local_var_61[2] = 46.1943893;
					local_var_61[3] = 0;
					local_var_61[4] = 0;
					local_var_61[5] = 0;
					local_var_61[6] = 0;
					local_var_61[7] = 0;
					local_var_61[8] = 0;
					local_var_61[9] = 0;
					local_var_61[10] = 0;
					local_var_61[11] = 0;
					local_var_62[0] = 0.970701993;
					local_var_62[1] = 0;
					local_var_62[2] = 0;
					local_var_62[3] = 0;
					local_var_65[0] = 56.2656898;
					local_var_65[1] = 3;
					local_var_65[2] = 46.1943893;
					local_var_65[3] = 0.970701993;
				}
				else
				{
					local_var_60 = 0;
					local_var_70 = 1;
					local_var_71 = 1;
					local_var_6d = 0;
					local_var_6f = 0;
					local_var_6b = 0;
					local_var_68 = 0;
					local_var_61[0] = 56.2656898;
					local_var_61[1] = 3;
					local_var_61[2] = 46.1943893;
					local_var_61[3] = 0;
					local_var_61[4] = 0;
					local_var_61[5] = 0;
					local_var_61[6] = 0;
					local_var_61[7] = 0;
					local_var_61[8] = 0;
					local_var_61[9] = 0;
					local_var_61[10] = 0;
					local_var_61[11] = 0;
					local_var_62[0] = 0.970701993;
					local_var_62[1] = 0;
					local_var_62[2] = 0;
					local_var_62[3] = 0;
					local_var_65[0] = 56.2656898;
					local_var_65[1] = 3;
					local_var_65[2] = 46.1943893;
					local_var_65[3] = 0.970701993;
				}
				break;
			case 14:
				local_var_60 = 9;
				local_var_70 = 3;
				local_var_71 = 3;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 65.236618;
				local_var_61[1] = 0;
				local_var_61[2] = 56.4658127;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 0.984839022;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 65.236618;
				local_var_65[1] = 0;
				local_var_65[2] = 56.4658127;
				local_var_65[3] = 0.984839022;
				break;
			case 15:
				local_var_60 = 9;
				local_var_70 = 1;
				local_var_71 = 1;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 64.1168213;
				local_var_61[1] = 0.624998987;
				local_var_61[2] = 39.7625237;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = 1.37454796;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 64.1168213;
				local_var_65[1] = 0.624998987;
				local_var_65[2] = 39.7625237;
				local_var_65[3] = 1.37454796;
				break;
			case 16:
				local_var_60 = 7;
				local_var_70 = 3;
				local_var_71 = 3;
				local_var_6d = 0;
				local_var_6f = 0;
				local_var_6b = 0;
				local_var_68 = 0;
				local_var_61[0] = 65.1943893;
				local_var_61[1] = 0;
				local_var_61[2] = 40.2632332;
				local_var_61[3] = 0;
				local_var_61[4] = 0;
				local_var_61[5] = 0;
				local_var_61[6] = 0;
				local_var_61[7] = 0;
				local_var_61[8] = 0;
				local_var_61[9] = 0;
				local_var_61[10] = 0;
				local_var_61[11] = 0;
				local_var_62[0] = -1.98034406;
				local_var_62[1] = 0;
				local_var_62[2] = 0;
				local_var_62[3] = 0;
				local_var_65[0] = 65.1943893;
				local_var_65[1] = 0;
				local_var_65[2] = 40.2632332;
				local_var_65[3] = -1.98034406;
				break;
		}
		if (file_var_41 == 1)
		{
			switch (local_var_5e)
			{
				case 4:
				case 13:
				case 5:
					return;
			}
		}
		usemapid(0);
		setpos(65.1906281, 0, 51.3104591);
		switch (local_var_60)
		{
			case 0:
				bindp2_d4(0x3000006, local_var_70, local_var_71);
				break;
			case 1:
				bindp2_d4(0x3000000, local_var_70, local_var_71);
				break;
			case 2:
				bindp2_d4(0x3000001, local_var_70, local_var_71);
				break;
			case 7:
				bindp2_d4(0x3000002, local_var_70, local_var_71);
				break;
			case 8:
				bindp2_d4(0x3000003, local_var_70, local_var_71);
				break;
			case 9:
				bindp2_d4(0x3000005, local_var_70, local_var_71);
				break;
		}
		setpos(local_var_61[0], local_var_61[1], local_var_61[2]);
		dir(local_var_62[0]);
		set_ignore_hitgroup(1);
		switch (local_var_5e)
		{
			case 2:
				setnpcname(75);
				reqenable(2);
				break;
			case 4:
				if (シナリオフラグ >= 0x1a4)
				{
					reqdisable(2);
				}
				else
				{
					setnpcname(71);
					reqenable(2);
				}
				break;
			case 5:
				setnpcname(112);
				reqenable(2);
				break;
			case 6:
				setnpcname(177);
				reqenable(2);
				break;
			case 8:
				setnpcname(85);
				reqenable(2);
				break;
			case 9:
				setnpcname(61);
				reqenable(2);
				break;
			case 12:
				setnpcname(44);
				reqenable(2);
				break;
			default:
				reqdisable(2);
				break;
		}
		if (local_var_5e <= 2)
		{
			usemapid(0);
		}
		else
		{
			switch (local_var_6f)
			{
				case 0:
					usemapid(0);
					break;
				default:
					usemapid(1);
					break;
			}
		}
		if (local_var_5e == 2)
		{
			settalkradiusoffset(1.79999995, 0, 0.699999988);
			talkradius(0.300000012);
		}
		if (local_var_5e == 9)
		{
		}
		if ((((local_var_60 == 7 || local_var_60 == 8) || local_var_5e == 13) || local_var_5e == 14))
		{
			setradius_221((getdefaultradiusw() * 0.699999988), (getdefaultradiusd() * 0.699999988));
		}
		else
		{
			setradius_221((getdefaultradiusw() * 0.800000012), (getdefaultradiusd() * 0.800000012));
		}
		setreachr(0.300000012);
		if ((((local_var_5e == 2 || local_var_5e == 7) || local_var_5e == 12) || local_var_5e == 3))
		{
			stdmotionread(17);
		}
		else
		{
			stdmotionread(16);
		}
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		local_var_64 = getdefaultwalkspeed();
		usecharhit(1);
		setweight(-1);
		//Couldn't get labels for REQ because either script or function is not immediate
		sysReq(1, getmyid(), 4);
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	float   local_var_63;            // pos: 0x60;
	float   local_var_66;            // pos: 0x78;
	float   local_var_67;            // pos: 0x7c;
	float   local_var_69;            // pos: 0x84;
	int     local_var_6a;            // pos: 0x98;
	int     local_var_6c;            // pos: 0xa0;
	u_char  local_var_6e;            // pos: 0xa5;

	function NPC挙動()
	{
		switch (local_var_5e)
		{
			case between(9, 11):
			case 13:
			case 15:
				break;
			default:
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				break;
		}
		switch (local_var_5e)
		{
			case 0:
				return;
			case 3:
				sysLookata(NPC08);
				setkutipakustatus(1);
				setunazukistatus(1);
				break;
			case 4:
				if (シナリオフラグ >= 0x1a4)
				{
					while (true)
					{
						sysLookata(NPC14);
						setkutipakustatus(1);
						setunazukistatus(1);
						stdmotionplay(0x1000010);
						wait(1);
						motionsync_282(1);
						setkutipakustatus(1);
						setunazukistatus(1);
						stdmotionplay(0x1000016);
						wait(1);
						wait(1);
						motionsync_282(1);
					}
				}
				while (true)
				{
					sysLookata(NPC14);
					motionplay_bb(0x10000000, 20);
					setkutipakustatus(1);
					setunazukistatus(1);
					wait(1);
					motionsync_282(1);
					wait(30);
					setkutipakustatus(0);
					setunazukistatus(0);
					motionplay_bb(0x10000001, 20);
					setkutipakustatus(1);
					setunazukistatus(1);
					wait(1);
					motionsync_282(1);
					wait(30);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				break;
			case 6:
			case 8:
				while (true)
				{
					setkubifuristatus(1);
					wait(60);
				}
			case 7:
				sysLookata(NPC04);
				setkutipakustatus(1);
				setunazukistatus(1);
				break;
			case 9:
			case 10:
			case 11:
				setautorelax(0);
				setweight(-1);
				setradius_221((getdefaultradiusw() * 0.600000024), (getdefaultradiusd() * 0.600000024));
				setpos(local_var_61[0], local_var_61[1], local_var_61[2]);
				dir(local_var_62[0]);
				switch (local_var_60)
				{
					case 1:
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x10000002, 0);
						break;
					case 2:
						motionloopframe(0, -1);
						motionloop(1);
						motionplay_bb(0x10000003, 0);
						break;
				}
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				if (local_var_5e != 11)
				{
					setkutipakustatus(1);
					setunazukistatus(1);
				}
				else
				{
					lookat_24f(61.9774361, 0.0250000004, 52.0844307);
				}
				break;
			case 13:
				if (シナリオフラグ >= 0x1a4)
				{
					setweight(-1);
					setautorelax(0);
					motionstartframe(0);
					motionloopframe(0, -1);
					motionloop(1);
					motionplay_bb(0x10000004, 0);
					rgbatrans(1, 1, 1, 0, 0);
					istouchucsync();
					clearhidecomplete();
					rgbatrans(1, 1, 1, 1, 10);
				}
				else
				{
					setweight(-1);
					stdmotionread(18);
					stdmotionreadsync();
					stdmotionvariation(1);
					stdmotionplay_2c2(0x1000000, 0);
					rgbatrans(1, 1, 1, 0, 0);
					istouchucsync();
					clearhidecomplete();
					rgbatrans(1, 1, 1, 1, 10);
					sysLookata(NPC05);
					setkutipakustatus(1);
					setunazukistatus(1);
				}
				break;
			case 14:
				lookat_24f(65.9641342, 1.34999895, 57.5602608);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch ((rand_29(100) % 4))
					{
						case 0:
							stdmotionplay(0x1000010);
							break;
						case 1:
							stdmotionplay(0x1000014);
							break;
						case 2:
							stdmotionplay(0x1000013);
							break;
						default:
							stdmotionplay(0x1000012);
							break;
					}
					wait(1);
					motionsync_282(1);
					wait((10 + rand_29(10)));
				}
			case 15:
				usecharhit(0);
				setautorelax(0);
				setweight(-1);
				motionloopframe(0, -1);
				motionloop(1);
				motionplay_bb(0x10000005, 0);
				rgbatrans(1, 1, 1, 0, 0);
				istouchucsync();
				clearhidecomplete();
				rgbatrans(1, 1, 1, 1, 10);
				sysLookata(NPC17);
				setkutipakustatus(1);
				setunazukistatus(1);
				break;
			case 16:
				setradius_221((getdefaultradiusw() * 0.600000024), (getdefaultradiusd() * 0.600000024));
				sysLookata(NPC16);
				setkutipakustatus(1);
				setunazukistatus(1);
				while (true)
				{
					switch (rand_29(1))
					{
						case 0:
							stdmotionplay(0x1000015);
							break;
						case 1:
							stdmotionplay(0x1000016);
							break;
						default:
							stdmotionplay(0x1000012);
							break;
					}
					wait(1);
					motionsync_282(1);
					wait((10 + rand_29(10)));
				}
			default:
				local_var_6e = 0;
				local_var_6a = 0;
				local_var_6c = 0;
				local_var_66 = 0.00600000005;
				local_var_67 = 0.00300000003;
				switch (local_var_6f)
				{
					case 0:
						usemapid(0);
						setweight(-1);
						stdmotionplay(0x1000000);
						while (true)
						{
							wait(0x12c);
						}
					case 1:
						local_var_6e = 0;
						local_var_6a = 0;
						local_var_6c = 0;
						local_var_66 = 0.00600000005;
						local_var_67 = 0.00300000003;
						motioncancel();
						stdmotionplay(0x1000000);
						while (true)
						{
							setreachr(0.100000001);
							local_var_69 = ((((local_var_68 - 1) * 0.800000012) + (((local_var_68 - 1) * (rand_29(100) % 20)) / 100)) + 1);
							local_var_62[1] = rand_29(100);
							local_var_62[1] = (local_var_62[1] / 100);
							local_var_62[2] = local_var_62[0];
							switch ((rand_29(100) % 2))
							{
								case 0:
									local_var_62[0] = normalang(((local_var_62[0] + 1.57079637) + (1.57079637 * local_var_62[1])));
									break;
								default:
									local_var_62[0] = normalang(((local_var_62[0] + -1.57079637) - (1.57079637 * local_var_62[1])));
									break;
							}
							if (local_var_6d != 0)
							{
								wait(local_var_6b);
								local_var_62[2] = normalang(((local_var_62[2] + 6.28318548) - (local_var_62[0] + 6.28318548)));
								if (!((local_var_62[2] >= -0.52359879 && local_var_62[2] <= 0.52359879)))
								{
									setaturnlookatlockstatus(1);
									aturn_261(local_var_62[0]);
								}
								wait(15);
							}
							else
							{
								wait((rand_29(30) + 40));
							}
							local_var_61[0] = (local_var_65[0] + (sin(local_var_62[0]) * local_var_69));
							local_var_61[1] = local_var_65[1];
							local_var_61[2] = (local_var_65[2] + (cos(local_var_62[0]) * local_var_69));
							local_var_63 = ((getlength3(local_var_61[0], local_var_61[1], local_var_61[2]) / local_var_64) + 1);
							setwalkspeed(local_var_64);
							stdmotionplay_2c2(0x1000000, 20);
							rmove(local_var_61[0], local_var_61[1], local_var_61[2]);
							local_var_6a = 0;
							while (!(local_var_6a >= local_var_63))
							{
								local_var_6a = (local_var_6a + 1);
								wait(1);
							}
							wait(1);
						}
					default:
						local_var_6e = 0;
						local_var_6a = 0;
						local_var_6c = 0;
						local_var_66 = 0.00600000005;
						local_var_67 = 0.00300000003;
						stdmotionplay(0x1000000);
						setpos(local_var_61[((local_var_6f - 1) * 3)], local_var_61[(((local_var_6f - 1) * 3) + 1)], local_var_61[(((local_var_6f - 1) * 3) + 2)]);
						while (true)
						{
							setreachr(0.100000001);
							if (local_var_64 <= 0)
							{
								wait(1);
							}
							else
							{
								setaturnlookatlockstatus(1);
								aturny(local_var_61[local_var_6e], local_var_61[(local_var_6e + 2)]);
								setwalkspeed(local_var_64);
								stdmotionplay_2c2(0x1000000, 20);
								move(local_var_61[local_var_6e], local_var_61[(local_var_6e + 1)], local_var_61[(local_var_6e + 2)]);
								if (local_var_6d != 0)
								{
									wait(local_var_6b);
								}
								local_var_6e = (local_var_6e + 3);
								if (local_var_6e >= (local_var_6f * 3))
								{
									local_var_6e = 0;
								}
								wait(1);
							}
						}
				}
				break;
		}
		return;
	}


	function ＮＰＣバインドオフ()
	{
		bindoff();
		return 0;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_5f;            // pos: 0x5;
	u_char  local_var_72;            // pos: 0xa9;
	u_char  local_var_74;            // pos: 0xb1;
	u_char  local_var_75;            // pos: 0xb2;

	function talk_step01()
	{
		if ((local_var_5e == 2 || local_var_5e == 9))
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			sysLookata(-1);
		}
		else if ((local_var_5e == 7 || local_var_5e == 12))
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
		}
		else
		{
			sethpmenu(0);
			ucoff();
			settrapshowstatus(0);
			setaturnlookatlockstatus(0);
			sysRaturna(-1);
			regI2 = 0;
			if (isturn())
			{
				turnsync();
				stdmotionplay_2c2(0x1000002, 20);
			}
			else
			{
				regI2 = 1;
			}
		}
		switch (local_var_5e)
		{
			case 2:
				if ((getclanrank() >= 4 && シナリオフラグ >= 0x4ba))
				{
					if (isquestclear(136))
					{
						local_var_74 = 5;
					}
					else if (g_btl_nm_マリリス == 2)
					{
						local_var_74 = 4;
					}
					else if (g_btl_nm_マリリス == 1)
					{
						local_var_74 = 3;
					}
					else if (isquestorder(136))
					{
						local_var_74 = 2;
					}
					else
					{
						local_var_74 = 1;
					}
				}
				else
				{
					local_var_74 = 0;
				}
				switch (local_var_74)
				{
					case 0:
					case gte(5):
						if (local_var_75 == 1)
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000000);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else if ((シナリオフラグ >= 0x604 && !(isquestclear(53))))
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000001);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else if ((シナリオフラグ >= 0x15e && シナリオフラグ < 0x1a4))
						{
							setkutipakustatus(1);
							setunazukistatus(1);
							amese(0, 0x1000002);
							messync(0, 1);
							setkutipakustatus(0);
							setunazukistatus(0);
						}
						else
						{
							switch (シナリオフラグ)
							{
								case lte(85):
									switch (local_var_5f)
									{
										case lte(0):
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000003);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											local_var_5f = (local_var_5f + 1);
											break;
										default:
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000003);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											break;
									}
									break;
								case lte(0x157):
									switch (local_var_5f)
									{
										case lte(0):
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000004);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											local_var_5f = (local_var_5f + 1);
											break;
										default:
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000004);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											break;
									}
									break;
								case lte(0x5f0):
									switch (local_var_5f)
									{
										case lte(0):
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000005);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											local_var_5f = (local_var_5f + 1);
											break;
										default:
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000005);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											break;
									}
									break;
								case lte(0x1004):
									switch (local_var_5f)
									{
										case lte(0):
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000006);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											local_var_5f = (local_var_5f + 1);
											break;
										default:
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000006);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											break;
									}
									break;
								default:
									switch (local_var_5f)
									{
										case lte(0):
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000007);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											local_var_5f = (local_var_5f + 1);
											break;
										default:
											setkutipakustatus(1);
											setunazukistatus(1);
											amese(0, 0x1000007);
											messync(0, 1);
											setkutipakustatus(0);
											setunazukistatus(0);
											break;
									}
									break;
							}
						}
						break;
					case 1:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000008);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
					case 2:
						setkutipakustatus(1);
						setunazukistatus(1);
						askpos(0, 0, 127);
						local_var_72 = aaske(0, 0x1000009);
						mesclose(0);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						switch (local_var_72)
						{
							case 0:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100000a);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								questeffectread(200);
								effectreadsync();
								effectplay(0);
								effectsync();
								g_btl_nm_マリリス = 1;
								setquestscenarioflag(136, 30);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100000c);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case 3:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000d);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
					case 4:
						setmesmacro(0, 0, 1, 0x808e);
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100000e);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						sysReq(1, 常駐監督::マリリスクリア);
						sysReqwait(常駐監督::マリリスクリア);
						subitem(0x808e, 1);
						local_var_75 = 1;
						break;
				}
				break;
			case 4:
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100000f);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100000f);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000010);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000010);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000011);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000011);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000012);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000012);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000013);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000013);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				break;
			case 5:
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000014);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000014);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000015);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000015);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000016);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000016);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000017);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000017);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000018);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000018);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				break;
			case 6:
				setkubifuristatus(0);
				if ((シナリオフラグ >= 0x15e && シナリオフラグ < 0x1a4))
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000019);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					switch (シナリオフラグ)
					{
						case lte(85):
							switch (local_var_5f)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001a);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_5f = (local_var_5f + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001a);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x157):
							switch (local_var_5f)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_5f = (local_var_5f + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001b);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x5f0):
							switch (local_var_5f)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001c);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_5f = (local_var_5f + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001c);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						case lte(0x1004):
							switch (local_var_5f)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001d);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_5f = (local_var_5f + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001d);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
						default:
							switch (local_var_5f)
							{
								case lte(0):
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001e);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									local_var_5f = (local_var_5f + 1);
									break;
								default:
									setkutipakustatus(1);
									setunazukistatus(1);
									amese(0, 0x100001e);
									messync(0, 1);
									setkutipakustatus(0);
									setunazukistatus(0);
									break;
							}
							break;
					}
				}
				setkubifuristatus(1);
				break;
			case 8:
				setkubifuristatus(0);
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100001f);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100001f);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000020);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000020);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000021);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000021);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000022);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000022);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000023);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000023);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				setkubifuristatus(1);
				break;
			case 9:
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000024);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000024);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000025);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000025);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000026);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000027);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000027);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000028);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000028);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				break;
			case 12:
				switch (シナリオフラグ)
				{
					case lte(85):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000029);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x1000029);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x157):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002a);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002a);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x5f0):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002b);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002b);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					case lte(0x1004):
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002c);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002c);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
					default:
						switch (local_var_5f)
						{
							case lte(0):
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002d);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								local_var_5f = (local_var_5f + 1);
								break;
							default:
								setkutipakustatus(1);
								setunazukistatus(1);
								amese(0, 0x100002d);
								messync(0, 1);
								setkutipakustatus(0);
								setunazukistatus(0);
								break;
						}
						break;
				}
				break;
		}
		if ((local_var_5e == 9 || local_var_5e == 2))
		{
			lookatoff();
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		else if ((local_var_5e == 7 || local_var_5e == 12))
		{
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
		}
		else
		{
			ucon();
			sethpmenu(1);
			clear_force_char_nearfade();
			setmaphighmodeldepth(-1);
			setmapmodelstatus(1);
			setstatuserrordispdenystatus(0);
			settrapshowstatus(1);
			stdmotionplay_2c2(0x1000000, 20);
		}
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_73;            // pos: 0xb0;

	function talkhold(16)
	{
		local_var_73 = 2;
		return;
	}


	function talkterm(17)
	{
		local_var_73 = 3;
		if (local_var_5e == 9)
		{
			lookatoff();
		}
		else if (!((local_var_5e == 7 || local_var_5e == 12)))
		{
			if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
			{
				setaturnlookatlockstatus(1);
				aturn_261(getdestrotangzx_227(0));
			}
		}
		local_var_73 = 0;
		return;
	}


	function 駆け上がるマスター()
	{
		move(53.4435501, 3.19998598, 36.0806999);
		move(51.8480606, 3.03122592, 36.0488319);
		setautorelax(0);
		motionloopframe(150, 209);
		motionloop(1);
		motionplay(0x10000006);
		return 0;
	}


	function PC向く()
	{
		regF0 = getrotangzx();
		setaturnlookatlockstatus(1);
		sysAturna(-1);
		return;
	}


	function 元の角度へ()
	{
		setaturnlookatlockstatus(1);
		aturn_261(regF0);
		return;
	}


	function マスター見る()
	{
		setkutipakustatus(0);
		setunazukistatus(0);
		sysLookata(NPC03);
		return;
	}


	function マスター見る解除して女見る()
	{
		sysLookata(NPC04);
		setkutipakustatus(1);
		setunazukistatus(1);
		return;
	}


	function NPC09移動()
	{
		setpos(70.6345596, 0, 50.1081161);
		return;
	}


	function NPC足音消し()
	{
		setcharseplay(0);
		return;
	}


	function NPC足音出し()
	{
		setcharseplay(1);
		return;
	}


	function NPC一時退避()
	{
		hidecomplete();
		while (file_var_44 != 1)
		{
			wait(1);
		}
		return;
	}


	function NPC一時退避復帰()
	{
		clearhidecomplete();
		return;
	}


	function 出発時移動()
	{
		movecancel();
		setpos(52.6967316, 3, 56.1595726);
		dir(2.59038997);
		sysLookata(ミュート);
		return 0;
		return;
	}
}


script ヴァン(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function 掲示板向く()
	{
		sysRlookatat(掲示板, 15);
		return;
	}


	function 掲示板向くから戻る()
	{
		sysRlookatat(トマジ, 13);
		return;
	}


	function 掲示板イベント時配置()
	{
		setpos(69.0931854, 0, 45.227066);
		dir(1.98699701);
		bindp2(0x3000007);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		usecharhit(0);
		motionloopframe(60, 120);
		motionloop(1);
		motionplay_bb(0x11000000, 0);
		return;
	}


	function 腕組み解く()
	{
		motionstartframe(120);
		motionloopframe(120, -1);
		motionloop(0);
		motionplay_bb(0x11000000, 0);
		motionsync();
		setaturnlookatlockstatus(1);
		sysAturna(トマジ);
		return;
	}


	function 腕組み解く2()
	{
		stdmotionplay_2c2(0x1000000, 0);
		sysDira(トマジ);
		lookat_maxvelr_limit_unlock();
		sysRlookatat(トマジ, 0);
		return;
	}


	function バインドオフ()
	{
		bindoff();
		return 0;
		return;
	}
}


script トマジ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_76;            // pos: 0x0;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		if ((!(isquestclear(128)) && g_btl_nm_パック == 2))
		{
			if (シナリオフラグ >= 0x15e)
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100002e);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				motionplay(0x12000000);
				wait(90);
				sysReq(1, 常駐監督::キラートマト撃破);
				sysReqwait(常駐監督::キラートマト撃破);
				if (!((g_iw_クラン情報フラグ[0] & 8)))
				{
					setmesmacro(0, 0, 1, 0x8071);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x100002f);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000030);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
			}
			else
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000031);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				motionplay(0x12000000);
				wait(90);
				sysReq(1, 常駐監督::キラートマト撃破);
				sysReqwait(常駐監督::キラートマト撃破);
				if (!((g_iw_クラン情報フラグ[0] & 8)))
				{
					setmesmacro(0, 0, 1, 0x8071);
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000032);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
				else
				{
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000033);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
				}
			}
			if (!(ismapjumpgroupflag(0x139)))
			{
				setmapjumpgroupflag(0x138);
				releasemapjumpgroupflag(0x139);
			}
		}
		else
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			askpos(0, 0, 127);
			local_var_76 = aaske(0, 0x1000034);
			mesclose(0);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
			switch (local_var_76)
			{
				case 0:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000035);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				case 1:
					setkutipakustatus(1);
					setunazukistatus(1);
					amese(0, 0x1000036);
					messync(0, 1);
					setkutipakustatus(0);
					setunazukistatus(0);
					break;
				default:
					break;
			}
			if (!(isquestclear(128)))
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000037);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
			}
			else if (!((g_iw_クラン情報フラグ[0] & 8)))
			{
				setmesmacro(0, 0, 1, 0x8071);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000038);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
			}
			else
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000039);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
			}
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}


	function 掲示板イベント時配置()
	{
		hidecomplete();
		setpos(68.8545609, 0, 43.2266159);
		dir(1.03064096);
		bindp2(0x3000008);
		hidecomplete();
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(46);
		motionread(2);
		motionreadsync(2);
		return;
	}


	function 掲示板イベント時表示()
	{
		clearhidecomplete();
		return;
	}


	function ちょっと歩み寄る()
	{
		setaturnlookatlockstatus(1);
		sysAturna(ヴァン);
		motionplay_bb(0x11000003, 5);
		return;
	}


	function クランレポート上げる()
	{
		setpos(68.8609238, 0, 43.9756432);
		sysDira(ヴァン);
		stdmotionplay_2c2(0x1000000, 0);
		return;
	}


	function 伝票あげる()
	{
		motionplay_bb(0x11000001, 5);
		return;
	}


	function 立ち()
	{
		setpos(68.8609238, 0, 43.9756432);
		sysDira(ヴァン);
		motionstartframe(170);
		motionloopframe(170, -1);
		motionloop(0);
		motionplay_bb(0x11000002, 0);
		wait(1);
		motionsync_282(1);
		return 0;
	}


	function トマジ執政官着任前位置()
	{
		hidecomplete();
		if (シナリオフラグ >= 0x1a4)
		{
			setpos(53.4511299, 3, 46.7748108);
			dir(-1.87293696);
		}
		else
		{
			setpos(60.4000015, 0, 44.9779778);
			dir(-0.838599026);
			sysReq(1, hitactor01_tmj::setHitObj);
		}
		bindp2(0x3000008);
		set_ignore_hitgroup(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setnpcname(46);
		motionread(2);
		motionreadsync(2);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		sysReq(1, トマジ::トマジ執政官着任前挙動);
		return;
	}


	function トマジ執政官着任前挙動()
	{
		while (true)
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			if (シナリオフラグ >= 0x1a4)
			{
				if (file_var_43 == 1)
				{
					sysLookata(NPC13);
				}
				else
				{
					lookat_24f(52.6180305, 3.02499604, 46.1134109);
				}
			}
			else
			{
				sysLookata(NPC13);
			}
			stdmotionplay(0x1000016);
			wait(1);
			motionsync_282(1);
			stdmotionplay(0x1000013);
			wait(1);
			wait(1);
			motionsync_282(1);
		}
	}
}


script ミュート(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function バルフレアバインド２()
	{
		usemapid(0);
		setpos(56.1081505, 2.51699495, 51.10149);
		dir(-1.384166);
		bindp2(0x3000009);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x13000000, 0);
		setweight(-1);
		if (シナリオフラグ == 0x186)
		{
			reqdisable(2);
		}
		else
		{
			stdmotionread(16);
			stdmotionreadsync();
			fieldsignmes(0x100003a);
			setautorelax(0);
			motionstartframe(0);
			motionloopframe(0, -1);
			motionloop(1);
			motionplay_bb(0x13000000, 0);
			setposoffset(-0.5, 0, 0, 3);
			talkradius(0.300000012);
			setradius_221(0.5, 0.699999988);
		}
		return;
	}


	function バルフレア進行確認バインド()
	{
		usemapid(0);
		setpos(69.5227432, 0, 54.9796791);
		dir(-2.23934388);
		bindp2(0x3000009);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		reqenable(2);
		fieldsignmes(0x100003a);
		setposoffset(0, 0, 0, 3);
		setradius_221(0.5, 0.5);
		sysReqchg(2, ミュート::バルフレアtalk);
		return;
	}


	function 消す()
	{
		bindoff();
		return 0;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_77;            // pos: 0x0;

	function バルフレアtalk_通常時()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		reqdisable(17);
		reqdisable(16);
		sysLookata(-1);
		g_iw_sb03_rbn_flg[0] = (g_iw_sb03_rbn_flg[0] | 2);
		switch (local_var_77)
		{
			case 0:
				fadeout(15);
				fadesync();
				g_com_navi_footcalc[0] = getx_12b(-1);
				g_com_navi_footcalc[1] = gety_12c(-1);
				g_com_navi_footcalc[2] = getz_12d(-1);
				setnavimapfootmarkstatus(0);
				sethpmenufast(0);
				settrapshowstatus(0);
				setcharseplayall(0);
				if (!(istownmap()))
				{
					setstatuserrordispdenystatus(1);
				}
				unkCall_5ac(1, 2.35800004, 100);
				FRAN.bonecalcforce(10);
				FRAN.lookat_maxvelr_limit_unlock();
				FRAN.rlookatt_258(55.9553947, 5, 51.9374962, 0);
				wait(5);
				camerastart_7e(0, 0x2000000);
				partyusemapid(1);
				setposparty(54.3289986, 3, 51.0268059, 2.02719903);
				FRAN.bonecalcforce(-1);
				sethpmenufast(0);
				settrapshowstatus(0);
				setcharseplayall(1);
				fadein(15);
				sysLookata(-1);
				乗っ取り１.capturepc(-1);
				乗っ取り１.sysLookata(ミュート);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100003b);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				wait(8);
				camerastart_7e(0, 0x2000001);
				乗っ取り１.sysLookata(FRAN);
				FRAN.setkutipakustatus(1);
				FRAN.setunazukistatus(1);
				FRAN.amese(0, 0x100003c);
				FRAN.messync(0, 1);
				FRAN.setkutipakustatus(0);
				FRAN.setunazukistatus(0);
				wait(8);
				乗っ取り１.sysLookata(ミュート);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100003d);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				乗っ取り１.lookatoff();
				乗っ取り１.releasepc();
				local_var_77 = (local_var_77 + 1);
				sysReq(1, FRAN::会話回数プラス);
				fadeout(15);
				fadesync();
				setcharseplayall(0);
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				setstatuserrordispdenystatus(0);
				unkCall_5ac(0, 0, 0);
				cameraclear();
				FRAN.lookatoff();
				wait(5);
				showparty();
				if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
				{
					clearnavimapfootmark();
				}
				setnavimapfootmarkstatus(1);
				setcharseplayall(1);
				sethpmenufast(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				wait(1);
				fadein(15);
				break;
			default:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100003e);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function バルフレアtalk()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		setkutipakustatus(1);
		setunazukistatus(1);
		amese(0, 0x100003f);
		messync(0, 1);
		setkutipakustatus(0);
		setunazukistatus(0);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		stdmotionplay_2c2(0x1000000, 20);
		return;
	}


	function talkhold(16)
	{
		return;
	}


	function talkterm(17)
	{
		if (((getrotangzx() - getdestrotangzx_227(0)) * (getrotangzx() - getdestrotangzx_227(0))) > 0)
		{
			setaturnlookatlockstatus(1);
			aturn_261(getdestrotangzx_227(0));
		}
		return;
	}
}


script FRAN(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function フランバインド()
	{
		hidecomplete();
		usemapid(0);
		setpos(55.1873589, 2.37497711, 49.0634422);
		dir(0.652939975);
		bindp2(0x300000a);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x13000001, 0);
		usemapid(0);
		setweight(-1);
		if (シナリオフラグ == 0x186)
		{
			reqdisable(2);
		}
		else
		{
			fieldsignmes(0x1000040);
			setposoffset(-0.300000012, 0, 0.300000012, 3);
			setradius_221(0.300000012, 0.200000003);
			talkradius(0.300000012);
		}
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function 消す()
	{
		bindoff();
		return 0;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_78;            // pos: 0x0;

	function talk_通常時()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		switch (local_var_78)
		{
			case 0:
				local_var_78 = (local_var_78 + 1);
				sysSysreq(1, ミュート::バルフレアバインド２);
				sysSysreqwait(ミュート::バルフレアバインド２);
				break;
			default:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000041);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function 会話回数プラス()
	{
		local_var_78 = (local_var_78 + 1);
		return;
	}


	function 出発だ！準備()
	{
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setpos(53.5800095, 3, 56.6260986);
		dir(2.12654495);
		usemapid(1);
		sysLookata(-1);
		return;
	}


	function 出発だ２()
	{
		sysLookata(ミュート);
		wait(25);
		movecancel();
		setwalkspeed(getdefaultwalkspeed());
		rmove(60.5374146, 0, 57.7710533);
		return;
	}
}


script ミゲロ(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}


	function ミゲロバインド()
	{
		hidecomplete();
		usemapid(0);
		setpos(54.4663849, 3, 50.8827248);
		dir(2.02738333);
		bindp2_d4(0x300000b, 1, 0);
		setreachr(0.300000012);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function 消す()
	{
		bindoff();
		return 0;
	}


	function 表示しない()
	{
		usemapid(0);
		usecharhit(0);
		hideshadow();
		reqdisable(2);
		hide();
		return;
	}


	function 表示する()
	{
		usemapid(1);
		usecharhit(1);
		showshadow();
		reqenable(2);
		show();
		setweight(-1);
		return;
	}


	function ミゲロトーク()
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		setaturnlookatlockstatus(0);
		sysRaturna(-1);
		regI2 = 0;
		if (isturn())
		{
			turnsync();
			stdmotionplay_2c2(0x1000002, 20);
		}
		else
		{
			regI2 = 1;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		aturn_261(getdestrotangzx_227(0));
		return;
	}
}


script 椅子１(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_79;            // pos: 0x0;

	function init()
	{
		local_var_79 = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function 椅子バインド()
	{
		usemapid(0);
		switch (local_var_79)
		{
			case 0:
				setpos(56.1465874, 3, 51.0858269);
				dir(-1.57077801);
				break;
			case 1:
				setpos(55.1025314, 3.0249939, 48.9639282);
				dir(-0.39268899);
				break;
		}
		bindp2(0x300000c);
		usemapid(0);
		setweight(-1);
		usecharhit(0);
		return;
	}


	function 椅子バインドオフ()
	{
		bindoff();
		return 0;
		return;
	}
}

symlink 椅子２(椅子１) : 0x1;


script 放浪ヴィエラ(6)
{

	function init()
	{
		return;
	}


	function vie_set()
	{
		hidecomplete();
		usemapid(0);
		setpos(56.8875885, 3.02499795, 52.6576576);
		dir(1.14592898);
		bindp2_d4(0x300000d, 4, 2);
		setnpcname(157);
		stdmotionread(18);
		stdmotionreadsync();
		stdmotionplay(0x1000000);
		setweight(-1);
		setautorelax(0);
		motionstartframe(0);
		motionloopframe(0, -1);
		motionloop(1);
		motionplay_bb(0x10000007, 0);
		setposoffset(-0.300000012, 0, 0, 3);
		setradius(0.400000006);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}


	function vie_action()
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_7a;            // pos: 0x0;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		switch (g_iw_ヴィエラ進行)
		{
			case 2:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000042);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				g_iw_ヴィエラ進行 = 3;
				break;
			case 3:
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000043);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				break;
			case 4:
				if (!((g_iw_ヴィエラのお相手[0] & 1)))
				{
					setaskselectignore(0, 0);
				}
				if (!((g_iw_ヴィエラのお相手[0] & 2)))
				{
					setaskselectignore(0, 1);
				}
				setkutipakustatus(1);
				setunazukistatus(1);
				askpos(0, 0, 127);
				local_var_7a = aaske(0, 0x1000044);
				mesclose(0);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				switch (local_var_7a)
				{
					case 0:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000045);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						g_iw_ヴィエラ進行 = 5;
						g_iw_ヴィエラのお相手[0] = (g_iw_ヴィエラのお相手[0] | 4);
						goto localjmp_14;
					case 1:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000045);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						g_iw_ヴィエラ進行 = 5;
						g_iw_ヴィエラのお相手[0] = (g_iw_ヴィエラのお相手[0] | 8);
						goto localjmp_14;
					default:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x1000046);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
				}
				break;
		}
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	localjmp_14:
		fadeout(15);
		fadesync();
		g_com_navi_footcalc[0] = getx_12b(-1);
		g_com_navi_footcalc[1] = gety_12c(-1);
		g_com_navi_footcalc[2] = getz_12d(-1);
		setnavimapfootmarkstatus(0);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(0);
		if (!(istownmap()))
		{
			setstatuserrordispdenystatus(1);
		}
		unkCall_5ac(1, 2.35800004, 100);
		setpos(62.1737175, 0, 55.0718536);
		dir(1.71047401);
		setwalkspeed(getdefaultrunspeed());
		partyusemapid(1);
		setposparty(56.3999977, 3, 53.9997292, 1.56393099);
		resetbehindcamera(1.99780202, 0.200000003);
		setnoupdatebehindcamera(1);
		stdmotionread(16);
		stdmotionreadsync();
		stdmotionplay_2c2(0x1000000, 0);
		wait(5);
		rmove(73.2636337, 0, 53.8250198);
		sethpmenufast(0);
		settrapshowstatus(0);
		setcharseplayall(1);
		fadein(15);
		wait(45);
		fadeout(15);
		fadesync();
		setcharseplayall(0);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		setstatuserrordispdenystatus(0);
		unkCall_5ac(0, 0, 0);
		bindoff();
		wait(15);
		sebsoundplay(1, 1);
		wait(15);
		sebsoundplay(1, 2);
		setnoupdatebehindcamera(0);
		wait(30);
		showparty();
		if (distance_290(-1, g_com_navi_footcalc[0], g_com_navi_footcalc[2]) >= 0.800000012)
		{
			clearnavimapfootmark();
		}
		setnavimapfootmarkstatus(1);
		setcharseplayall(1);
		sethpmenufast(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		wait(1);
		fadein(15);
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}
}


script 乗っ取り１(6)
{
//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	int     local_var_7b;            // pos: 0x0;

	function init()
	{
		local_var_7b = getduplicateid();
		return;
	}


	function main(1)
	{
		return;
	}


	function talk(2)
	{
		return;
	}


	function 乗っ取る()
	{
		capturepc(-1);
		return;
	}


	function 乗っ取り解除()
	{
		lookatoff();
		releasepc();
		return 0;
	}


	function 操作キャラ乗っ取り()
	{
		capturepc(-1);
		return;
	}
}


script 依頼人(6)
{

	function init()
	{
		return;
	}


	function main(1)
	{
		return;
	}

//======================================================================
//                    Local (script) scope variables                    
//======================================================================
	u_char  local_var_7c;            // pos: 0x1;

	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		sysLookata(-1);
		if (isquestorder(129))
		{
			if (g_btl_nm_テクスタ == 2)
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000047);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				sysReq(1, 常駐監督::テクスタ撃破);
				sysReqwait(常駐監督::テクスタ撃破);
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x1000048);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				fadeout(30);
				fadesync();
				bindoff();
				wait(15);
				fadein(30);
				ucon();
				sethpmenu(1);
				clear_force_char_nearfade();
				setmaphighmodeldepth(-1);
				setmapmodelstatus(1);
				setstatuserrordispdenystatus(0);
				settrapshowstatus(1);
				return;
			}
			if (g_btl_nm_テクスタ != 1)
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				askpos(0, 0, 127);
				local_var_7c = aaske(0, 0x1000049);
				mesclose(0);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
				switch (local_var_7c)
				{
					case 0:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100004a);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						questeffectread(193);
						effectreadsync();
						effectplay(0);
						effectsync();
						g_btl_nm_テクスタ = 1;
						setquestscenarioflag(129, 30);
						break;
					default:
						setkutipakustatus(1);
						setunazukistatus(1);
						amese(0, 0x100004b);
						messync(0, 1);
						setkutipakustatus(0);
						setunazukistatus(0);
						break;
				}
			}
			else
			{
				setkutipakustatus(1);
				setunazukistatus(1);
				amese(0, 0x100004c);
				messync(0, 1);
				setkutipakustatus(0);
				setunazukistatus(0);
			}
		}
		else
		{
			setkutipakustatus(1);
			setunazukistatus(1);
			amese(0, 0x100004d);
			messync(0, 1);
			setkutipakustatus(0);
			setunazukistatus(0);
		}
		lookatoff();
		ucon();
		sethpmenu(1);
		clear_force_char_nearfade();
		setmaphighmodeldepth(-1);
		setmapmodelstatus(1);
		setstatuserrordispdenystatus(0);
		settrapshowstatus(1);
		return;
	}


	function 依頼人配置()
	{
		hidecomplete();
		usemapid(0);
		setpos(70.2165375, 0, 48.6176872);
		dir(-1.40315199);
		bindp2_d4(0x3000000, 5, 1);
		set_ignore_hitgroup(1);
		stdmotionread(18);
		stdmotionreadsync();
		stdmotionplay_2c2(0x1000000, 0);
		setweight(-1);
		puppetbind(12, 0x300000e, 1, 2);
		puppetsetpos(12, 70.8787613, 0, 48.076458);
		puppetsetscale(12, 1.70000005);
		setnpcname(123);
		setradius_221(0.600000024, 1);
		rgbatrans(1, 1, 1, 0, 0);
		istouchucsync();
		clearhidecomplete();
		rgbatrans(1, 1, 1, 1, 10);
		return;
	}
}


script hitactor01(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(64.5810318, 0, 39.8227158);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(64.5810318, 0, 39.8227158);
		dir(-0.983759999);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.600000024, 0.600000024);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor02(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(60.1581001, 0, 37.1928558);
		dir(-1.58574903);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(60.1581001, 0, 37.1928558);
		dir(-1.58574903);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(1.89999998, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor03(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(65.6099167, 0, 57.0097313);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(65.6099167, 0, 57.0097313);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_sol(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(56.3999977, 3, 46.9387321);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(56.3999977, 3, 46.9387321);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor02_sol(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(56.514122, 3, 48.522438);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(56.514122, 3, 48.522438);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor03_sol(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(56.5991325, 3, 45.7741737);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(56.5991325, 3, 45.7741737);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor01_tmj(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(60.1288223, 0, 45.5449371);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(60.1288223, 0, 45.5449371);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.800000012, 0.800000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script hitactor_cup(6)
{

	function init()
	{
		return;
	}


	function setHitObj()
	{
		usemapid(0);
		setpos(70.8787613, 0, 48.076458);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.300000012, 0.300000012);
		setheight(0.800000012);
		setweight(-1);
		return;
	}


	function setHitObj_omoi()
	{
		usemapid(0);
		setpos(70.8787613, 0, 48.076458);
		dir(-1.34032094);
		bindhitobj();
		set_ignore_hitgroup(1);
		setradius_221(0.300000012, 0.300000012);
		setheight(0.800000012);
		setweight(-1);
		weight_upper_pc();
		return;
	}


	function clearHitObj()
	{
		bindoff();
		return;
	}
}


script map_ダミーＰＣ(6)
{

	function init()
	{
		return;
	}


	function 扉オープン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function アイテム１()
	{
		capturepc(-1);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcerelax(0);
		motionsync_282(1);
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック1()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ターン()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function 扉オープン１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		setforcerelax(1);
		stdmotioneventsync();
		wait(8);
		return;
	}


	function 扉オープン２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ギミックアタックxyz()
	{
		capturepc(-1);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタック１xyz()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタック２xyz()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function ギミックアタックxyz1()
	{
		capturepc(-1);
		mp_map_set_angle = getangle(mp_map_set_x, mp_map_set_z);
		rlookat(mp_map_set_angle);
		turn_62(mp_map_set_angle);
		turnsync();
		setforcefight(-1);
		motionsync_282(1);
		stdmotionplay(0x100001c);
		stdmotioneventsync();
		if (mp_map_set_hitse != -1)
		{
			mapsoundplay(mp_map_set_hitse);
		}
		vibplay(6);
		wait(6);
		vibstop();
		return;
	}


	function ギミックアタックxyz2()
	{
		motionsync_282(1);
		clearforcefight();
		motionsync_282(1);
		releasepc();
		return;
	}


	function アイテム１xyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		stdmotionplay_2c2(0x100001b, 6);
		stdmotioneventsync();
		return;
	}


	function アイテム２xyz()
	{
		motionsync_282(1);
		clearforcerelax();
		motionsync_282(1);
		lookatoff();
		releasepc();
		return;
	}


	function ターンxyz()
	{
		capturepc(-1);
		rlookat_257(mp_map_set_x, (mp_map_set_y + -1.20000005), mp_map_set_z);
		turn(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		turnsync();
		lookatoff();
		releasepc();
		return;
	}


	function map_behind_return()
	{
		capturepc(-1);
		setnoupdatebehindcamera(0);
		releasepc();
		return;
	}


	function ＳＥＴＰＯＳxyz()
	{
		capturepc(-1);
		setpos(mp_map_set_x, mp_map_set_y, mp_map_set_z);
		releasepc();
		return;
	}


	function ＳＥＴＴＵＲＮxyz()
	{
		capturepc(-1);
		turnt(mp_map_set_x, mp_map_set_y, mp_map_set_z, 0);
		releasepc();
		return;
	}
}

#ifdef DEBUG
#define MES_POSITION_DIALOG (0x1000000 | 161)
#include "../../message_test/dst_a01.ebp_unc/position.c"
#endif //DEBUG
#include "include/friends_npc.c"

script PC00(7) : 0x80
{

	function init()
	{
		setupbattle(0);
		return;
	}
}


script PC01(7) : 0x81
{

	function init()
	{
		setupbattle(1);
		return;
	}
}


script PC02(7) : 0x82
{

	function init()
	{
		setupbattle(2);
		return;
	}
}


script PC03(7) : 0x83
{

	function init()
	{
		setupbattle(3);
		return;
	}
}

//======================================================================
//                           Map exit arrays                            
//======================================================================

mapExitArray mapExitGroup0[1] = {

	exitStruct mapExit0 = {
		75, 0, 51.1111107, 1, 
		75, 0, 54.8888893, 0x140f
	};

};

mapExitArray mapExitGroup1[0] = {

};

mapExitArray mapExitGroup2[0] = {

};

mapExitArray mapExitGroup3[0] = {

};

mapExitArray mapExitGroup4[0] = {

};


//======================================================================
//                      Map jump position vectors                       
//======================================================================
mjPosArr1 mapJumpPositions1[1] = {

	mjPos mapJumpPos0 = {
		70, 0, 53, -1.57079589,
		0, 0, 0, 0
	};

};
mjPosArr2 mapJumpPositions2[1] = {

	mjPos mapJumpPos0 = {
		70, 0, 53, -1.57079589,
		0, 0, 0, 0
	};

};


//======================================================================
//                       Unknown position arrays                        
//======================================================================
unkPos1 unknownPosition0[7] = {75, 0, 53, 3.98510432, 3.61029267, 0, 0};
unkPos1 unknownPosition1[7] = {75, 0, 53, 0.64509809, 1.38807034, 0, 0};

unkPos2 unknownPosition0[7] = {75, 1.20000005, 53, 1, 1, 0, 0};


//======================================================================
//                          Unknown u16 Arrays                          
//======================================================================
unk16Arr1 unknown16Arrays1[1] = {
	unk16ArrEntry unknown16Array0 = {0, 1, 0, 0, 1, 0, 0, 0};
};
unk16Arr2 unknown16Arrays2[21] = {
	unk16ArrEntry unknown16Array0 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array1 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array2 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array3 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array4 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array5 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array6 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array7 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array8 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array9 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array10 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array11 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array12 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array13 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array14 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array15 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array16 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array17 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array18 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array19 = {0, 0, 0, 0, 0, 65535, 0, 0};
	unk16ArrEntry unknown16Array20 = {0, 1, 0, 0, 1, 65535, 0, 0};
};
unk16Arr3 unknown16Arrays3[6] = {
	unk16ArrEntry unknown16Array0 = {0, 768, 1, 1};
	unk16ArrEntry unknown16Array1 = {1, 768, 1, 1};
	unk16ArrEntry unknown16Array2 = {2, 768, 1, 1};
	unk16ArrEntry unknown16Array3 = {3, 768, 1, 1};
	unk16ArrEntry unknown16Array4 = {4, 768, 1, 1};
	unk16ArrEntry unknown16Array5 = {5, 768, 1, 1};
};

//======================================================================
//                       REQALL/REQWAITALL arrays                       
//======================================================================
reqa   reqArr0[] = {
	椅子１::椅子バインド,
	椅子２::椅子バインド
};

reqa   reqArr1[] = {
	NPC02::NPC足音消し,
	NPC03::NPC足音消し,
	NPC04::NPC足音消し,
	NPC05::NPC足音消し,
	NPC06::NPC足音消し,
	NPC07::NPC足音消し,
	NPC08::NPC足音消し,
	NPC09::NPC足音消し,
	NPC10::NPC足音消し,
	NPC11::NPC足音消し,
	NPC12::NPC足音消し,
	NPC13::NPC足音消し,
	NPC14::NPC足音消し,
	NPC15::NPC足音消し,
	NPC16::NPC足音消し,
	NPC17::NPC足音消し,
	NPC01::NPC足音消し
};

reqa   reqArr2[] = {
	NPC02::NPC配置,
	NPC03::NPC配置,
	NPC04::NPC配置,
	NPC05::NPC配置,
	NPC06::NPC配置,
	NPC07::NPC配置,
	NPC08::NPC配置,
	NPC09::NPC配置,
	NPC10::NPC配置,
	NPC11::NPC配置,
	NPC12::NPC配置,
	NPC13::NPC配置,
	NPC14::NPC配置,
	NPC15::NPC配置,
	NPC16::NPC配置,
	NPC17::NPC配置,
	NPC01::NPC配置
};

reqa   reqArr3[] = {
	NPC02::NPC一時退避,
	NPC03::NPC一時退避,
	NPC04::NPC一時退避,
	NPC05::NPC一時退避,
	NPC06::NPC一時退避,
	NPC07::NPC一時退避,
	NPC08::NPC一時退避,
	NPC09::NPC一時退避,
	NPC10::NPC一時退避,
	NPC11::NPC一時退避,
	NPC12::NPC一時退避,
	NPC13::NPC一時退避,
	NPC14::NPC一時退避,
	NPC15::NPC一時退避,
	NPC16::NPC一時退避,
	NPC17::NPC一時退避,
	NPC01::NPC一時退避
};

reqa   reqArr4[] = {
	NPC02::NPC一時退避復帰,
	NPC03::NPC一時退避復帰,
	NPC04::NPC一時退避復帰,
	NPC05::NPC一時退避復帰,
	NPC06::NPC一時退避復帰,
	NPC07::NPC一時退避復帰,
	NPC08::NPC一時退避復帰,
	NPC09::NPC一時退避復帰,
	NPC10::NPC一時退避復帰,
	NPC11::NPC一時退避復帰,
	NPC12::NPC一時退避復帰,
	NPC13::NPC一時退避復帰,
	NPC14::NPC一時退避復帰,
	NPC15::NPC一時退避復帰,
	NPC16::NPC一時退避復帰,
	NPC17::NPC一時退避復帰,
	NPC01::NPC一時退避復帰
};

reqa   reqArr5[] = {
	椅子１::椅子バインドオフ,
	椅子２::椅子バインドオフ
};

reqa   reqArr6[] = {
};


