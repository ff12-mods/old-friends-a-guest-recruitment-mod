#define PI  3.14159265359

#define MODEL_MASK 0x3000000
#define NPC_START_GID 17

#define NPC_REDDAS 0
#define NPC_BASCH 1
#define NPC_LARSA 2
#define NPC_VOSSLER 3
#define NPC_AMALIA 4
#define NPC_REKS 5

#define MES_MASK 0x1000000
#define NPC_NAME_START 162
#define NPC_RECRUIT_START 172

#include "sys/pad.h"
#include "sys/message.h"

#ifndef GAME_IZJS
	#define CHAR_OFFSET		0x1c8
	#define CHAR_DATA_START 0x4e494
	#define CHAR_LP_START 0x4e624
	#define LICENSE_ARR_START 0x4e628
	#define EQUIP_ARR_START 0x4e4e4
#else //GAME_IZJS
	#define CHAR_OFFSET		0x1c4
	#define CHAR_DATA_START 0x2008
	#define CHAR_LP_START 0x2198
	#define LICENSE_ARR_START 0x219c
	#define EQUIP_ARR_START 0x2058
#endif //GAME_IZJS

import global int		vaanLP			= CHAR_LP_START + 0 * CHAR_OFFSET;
import global int		reksLP			= CHAR_LP_START + 6 * CHAR_OFFSET;
/**
 7 - amalia
 8 - bash prison
 9 - bash prison 2
10 - lamont
11 - Vossler
12 - Vossler 2
13 - Larsa
14 - Reddas
15 - Basch
16 - Reddas/Basch
 */

float baschRedPos[8] = {
	60.4, 0, 44.2, -2.0*PI/3.0, //reddas
	60.4, 0, 42.7, -PI/3.0 //basch
};

symlink guestBasch(guestReddas) : 1;

script guestReddas(6)
{
	int duplicateId;
	function init()
	{
		duplicateId = getduplicateid();
		setpos(baschRedPos[duplicateId*4 +0], baschRedPos[duplicateId*4 +1], baschRedPos[duplicateId*4 +2]);
		dir(baschRedPos[duplicateId*4 +3]);
		bindp2(MODEL_MASK | (NPC_START_GID + duplicateId));
		setweight(-1);
		set_ignore_hitgroup(1);
		//setnpcname(1142 + duplicateId);
		fieldsignmes(MES_MASK | (NPC_NAME_START + duplicateId));
		stdmotionread(0);
		stdmotionreadsync();
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);

		switch(duplicateId)
		{
			case NPC_REDDAS:
				sysLookata(guestBasch);
				setunazukistatus(1);
				break;
			case NPC_BASCH:
				sysLookata(guestReddas);
				setunazukistatus(1);
				break;
		}
		return;
	}

	function main(1)
	{
		while (true)
		{
			setkutipakustatus(1);
			wait(30 + rand_29(30));
			setkutipakustatus(0);
			wait(20 + rand_29(40));
		}
		return;
	}

	float currAngle;
	int recruitChoice;
	int guestId;
	function talk(2)
	{
		if (duplicateId == 0)
			guestId = 14;
		else
			guestId = 9;
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		currAngle = getrotangzx();
		sysRaturna(-1);
		if (isturn())
			turnsync();

		if (isbattlemember(guestId))
		{
			setaskselectignore(0, 0);
			askpos(0, 0, 2);
		}
		else
		{
			setaskselectignore(0, 1);
			setaskselectignore(0, 2);
			askpos(0, 0, 1);
		}
		
		recruitChoice = aaske(0, MES_MASK | (NPC_RECRUIT_START + duplicateId));
		mesclose(0);
		messync(0, 1);
		switch (recruitChoice)
		{
			case 0:
				addGuestIdNumber = guestId;
				sysReqew(0, addGuest::addReplaceGuest);
				break;
			case 1:
				removeGuestIdNumber = guestId;
				sysReqew(0, addGuest::removeGuest);
				break;
			case 2:
				modifyEquipmentId = guestId;
				sysReqew(0, modifyEquip::displayMessage);
				ucon();
				sysReqew(0, modifyEquip::changeEquipment);
				break;
		}

		aturn_261(currAngle);
		settrapshowstatus(1);
		ucon();
		sethpmenu(1);
		
		return;
	}
}

#define MES_LARSA_RECRUIT 171
char larsaAmaliaTalk = false;
script guestLarsa(6)
{
	char onTheWay = false;
	float npcPos[4] = {66.0, 0, 56.0, -PI/5.0};
	function init()
	{
		larsaAmaliaTalk = false;
		onTheWay = false;
		setpos(npcPos[0], npcPos[1], npcPos[2]);
		dir(npcPos[3]);
		bindp2(MODEL_MASK | (NPC_START_GID + NPC_LARSA));
		setweight(-1);
		set_ignore_hitgroup(1);
		//setnpcname(1142 + NPC_LARSA);
		fieldsignmes(MES_MASK | (NPC_NAME_START + NPC_LARSA));
		stdmotionread(0);
		stdmotionreadsync();
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		setunazukistatus(1);
		return;
	}

	function main(1)
	{
		wait(10);
		sysRaturna(NPC15);
		sysLookata(NPC15);
		NPC15.sysRaturna(guestLarsa);
		NPC15.sysLookata(guestLarsa);
		while (true)
		{
			setkutipakustatus(1);
			wait(30 + rand_29(30));
			setkutipakustatus(0);
			wait(20 + rand_29(40));
			if ((rand_29(100) % 6) == 0)
			{
				onTheWay = true;
				sysReqew(1, guestLarsa::gotoAmalia);
				sysReqew(1, guestLarsa::returnToUsualPlace);
				onTheWay = false;
			}
		}
		return;
	}

	float currAngle;
	int recruitChoice;
	int guestId = 13;
	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);

		if (!onTheWay)
		{
			NPC15.aturn_261(0.984839022);
			NPC15.lookat_24f(65.9641342, 1.34999895, 57.5602608);
		}
		else
		{
			currAngle = getrotangzx();
		}
		sysRaturna(-1);
		if (isturn())
			turnsync();
		
		setkutipakustatus(1);
		setunazukistatus(1);
		
		if (isbattlemember(guestId))
		{
			setaskselectignore(0, 0);
			askpos(0, 0, 2);
		}
		else
		{
			setaskselectignore(0, 1);
			setaskselectignore(0, 2);
			askpos(0, 0, 1);
		}

		recruitChoice = aaske(0, MES_MASK | (NPC_RECRUIT_START + NPC_LARSA));
		mesclose(0);
		messync(0, 1);
		switch (recruitChoice)
		{
			case 0:
				addGuestIdNumber = guestId;
				sysReqew(0, addGuest::addReplaceGuest);
				break;
			case 1:
				removeGuestIdNumber = guestId;
				sysReqew(0, addGuest::removeGuest);
				break;
			case 2:
				modifyEquipmentId = guestId;
				sysReqew(0, modifyEquip::displayMessage);
				ucon();
				sysReqew(0, modifyEquip::changeEquipment);
				break;
		}
		
		setkutipakustatus(0);
		setunazukistatus(0);
	   
		if (!onTheWay)
		{
			sysRaturna(NPC15);
			sysLookata(NPC15);
			NPC15.sysRaturna(guestLarsa);
			NPC15.sysLookata(guestLarsa);
		}
		else
		{
			aturn_261(currAngle);
		}

		settrapshowstatus(1);
		ucon();
		sethpmenu(1);

		return;
	}

	function gotoAmalia()
	{
		NPC15.aturn_261(0.984839022);
		NPC15.lookat_24f(65.9641342, 1.34999895, 57.5602608);

		setunazukistatus(0);
		setkutipakustatus(0);

		lookatoff();
		setkubifuristatus(1);
		
		//go to amalia
		move(68.5, 0.0, 49.5);
		move(67.1, 0.0, 41.84);
		move(67.02, 0.0, 38.8);

		setkubifuristatus(0);
		
		guestAmalia.sysLookata(guestLarsa);
		larsaAmaliaTalk = true;
		sysRaturna(guestAmalia);
		sysLookata(guestAmalia);

		setunazukistatus(1);
		guestAmalia.setunazukistatus(1);
		guestAmalia.setkutipakustatus(1);

		while (true)
		{
			setkutipakustatus(1);
			wait(50 + rand_29(30));
			setkutipakustatus(0);
			wait(50 + rand_29(40));
			if ((rand_29(100) % 6) == 0)
			{
				break;
			}
		}

		guestAmalia.setunazukistatus(0);
		guestAmalia.setkutipakustatus(0);
		guestAmalia.lookatoff();
		larsaAmaliaTalk = false;
		return;
	}

	function returnToUsualPlace()
	{
		setunazukistatus(0);
		setkutipakustatus(0);

		lookatoff();
		setkubifuristatus(1);
		
		move(67.1, 0.0, 41.84);
		move(68.5, 0.0, 49.5);
		move(npcPos[0], npcPos[1], npcPos[2]);

		setkubifuristatus(0);
		
		sysRaturna(NPC15);
		sysLookata(NPC15);
		NPC15.sysRaturna(guestLarsa);
		NPC15.sysLookata(guestLarsa);

		setunazukistatus(1);
		return;
	}
}

script guestVossler(6)
{
	float npcPos[4] = {56.4, 3.0, 52.4, PI/2.0};
	char talkingWithBash = false;
	function init()
	{
		setpos(npcPos[0], npcPos[1], npcPos[2]);
		dir(npcPos[3]);
		bindp2(MODEL_MASK | (NPC_START_GID + NPC_VOSSLER));
		setweight(-1);
		set_ignore_hitgroup(1);
		fieldsignmes(MES_MASK | (NPC_NAME_START + NPC_VOSSLER));
		stdmotionread(0);
		stdmotionreadsync();
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		return;
	}

	function main(1)
	{
		wait(10);
		sysLookata(guestAmalia);
		while(true)
		{
			if ((rand_29(100) % 4) == 0)
			{
				sysReqew(1, guestVossler::goToBasch);
				sysReqew(1, guestVossler::returnToUsualPlace);
			}
			wait(70);
		}
		return;
	}

	float currAngle;
	int recruitChoice;
	int guestId = 12;
	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		currAngle = getrotangzx();
		sysRaturna(-1);
		if (isturn())
			turnsync();

		if (isbattlemember(guestId))
		{
			setaskselectignore(0, 0);
			askpos(0, 0, 2);
		}
		else
		{
			setaskselectignore(0, 1);
			setaskselectignore(0, 2);
			askpos(0, 0, 1);
		}
		
		recruitChoice = aaske(0, MES_MASK | (NPC_RECRUIT_START + NPC_VOSSLER));
		mesclose(0);
		messync(0, 1);
		switch (recruitChoice)
		{
			case 0:
				addGuestIdNumber = guestId;
				sysReqew(0, addGuest::addReplaceGuest);
				break;
			case 1:
				removeGuestIdNumber = guestId;
				sysReqew(0, addGuest::removeGuest);
				break;
			case 2:
				modifyEquipmentId = guestId;
				sysReqew(0, modifyEquip::displayMessage);
				ucon();
				sysReqew(0, modifyEquip::changeEquipment);
				break;
		}

		aturn_261(currAngle);
		settrapshowstatus(1);
		ucon();
		sethpmenu(1);
		return;
	}

	function goToBasch()
	{
		lookatoff();
		setkubifuristatus(1);
		
		//go to basch
		move(54.0, 3.0, 54.25);
		move(54.92, 3.0, 56.86);
		move(59.14, 0.0, 56.0);
		move(60.8, 0.0, 51.0);
		move(62.37, 0.0, 46.48);
		move(62.37, 0.0, 43.47);
		move(61.44, 0.0, 43.42);
		
		sysRaturna(guestBasch);
		sysLookata(guestBasch);
		guestBasch.sysRturna(guestVossler);
		guestReddas.sysRturna(guestVossler);
		guestBasch.sysLookata(guestVossler);
		guestReddas.sysLookata(guestVossler);

		setkubifuristatus(0);
		setunazukistatus(1);
		setkutipakustatus(1);

		//talk with basch
		while(true)
		{
			wait(40);
			if ((rand_29(100) % 5) == 0)
				break;
		}

		//switch to reddas
		sysRaturna(guestReddas);
		sysLookata(guestReddas);

		//talk with reddas
		while(true)
		{
			wait(40);
			if ((rand_29(100) % 5) == 0)
				break;
		}

		//release reddas and basch
		guestBasch.aturn_261(baschRedPos[7]);
		guestReddas.aturn_261(baschRedPos[3]);
		guestBasch.sysLookata(guestReddas);
		guestReddas.sysLookata(guestBasch);

		//get back to usual place
		return;
	}

	function returnToUsualPlace()
	{
		lookatoff();
		setunazukistatus(0);
		setkutipakustatus(0);
		setkubifuristatus(1);
		
		//go back
		move(62.37, 0.0, 43.47);
		move(62.37, 0.0, 46.48);
		move(60.8, 0.0, 51.0);
		move(59.14, 0.0, 56.0);
		move(54.92, 3.0, 56.86);
		move(54.0, 3.0, 54.25);
		move(54.0, 3.0, 54.25);
		move(npcPos[0], npcPos[1], npcPos[2]);
		setkubifuristatus(0);
		
		aturn_261(npcPos[3]);
		sysLookata(guestAmalia);
		return;
	}
}

script guestAmalia(6)
{
	float npcPos[4] = {67.1, 0, 37.2, 0};
	function init()
	{
		setpos(npcPos[0], npcPos[1], npcPos[2]);
		dir(npcPos[3]);
		bindp2(MODEL_MASK | (NPC_START_GID + NPC_AMALIA));
		setweight(-1);
		set_ignore_hitgroup(1);
		fieldsignmes(MES_MASK | (NPC_NAME_START + NPC_AMALIA));
		stdmotionread(16);
		stdmotionreadsync();
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		return;
	}

	function main(1)
	{
		while (true)
		{
			if (!larsaAmaliaTalk)
			{
				switch ((rand_29(100) % 5))
				{
					case 0:
						stdmotionplay(0x1000015);
						break;
					case 1:
						stdmotionplay(0x1000012);
						break;
					default:
						stdmotionplay(0x1000000);
				}
				wait(1);
				motionsync_282(1);
				wait((20 + rand_29(20)));
			}
			else
			{
				switch ((rand_29(100) % 5))
				{
					case 0:
						stdmotionplay(0x1000015);
						break;
					case 1:
						stdmotionplay(0x1000016);
						break;
					case 2:
						stdmotionplay(0x1000013);
						break;
					default:
						stdmotionplay(0x1000014);
				}
				wait(1);
				motionsync_282(1);
				wait((10 + rand_29(20)));
			}
		}
		return;
	}

	float currAngle;
	int recruitChoice;
	int guestId = 7;
	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		currAngle = getrotangzx();
		sysRaturna(-1);
		setkutipakustatus(1);
		setunazukistatus(1);
		if (isturn())
			turnsync();

		if (isbattlemember(guestId))
		{
			setaskselectignore(0, 0);
			askpos(0, 0, 2);
		}
		else
		{
			setaskselectignore(0, 1);
			setaskselectignore(0, 2);
			askpos(0, 0, 1);
		}

		recruitChoice = aaske(0, MES_MASK | (NPC_RECRUIT_START + NPC_AMALIA));
		mesclose(0);
		messync(0, 1);
		switch (recruitChoice)
		{
			case 0:
				addGuestIdNumber = guestId;
				sysReqew(0, addGuest::addReplaceGuest);
				break;
			case 1:
				removeGuestIdNumber = guestId;
				sysReqew(0, addGuest::removeGuest);
				break;
			case 2:
				modifyEquipmentId = guestId;
				sysReqew(0, modifyEquip::displayMessage);
				ucon();
				sysReqew(0, modifyEquip::changeEquipment);
				break;
		}
		if (!larsaAmaliaTalk)
		{
			setunazukistatus(0);
			setkutipakustatus(0);
		}
		aturn_261(currAngle);
		settrapshowstatus(1);
		ucon();
		sethpmenu(1);
		return;
	}
}

#ifdef ADD_REKS
script guestReks(6)
{
	int file_var_0; int file_var_1; int file_var_2; int file_var_3; int file_var_4;
	float npcPos[4] = {61.5, 0, 58.3, -2.0*PI/3.0};
	function init()
	{
		setpos(npcPos[0], npcPos[1], npcPos[2]);
		dir(npcPos[3]);
		bindp2(MODEL_MASK | (NPC_START_GID + NPC_REKS));
		setweight(-1);
		set_ignore_hitgroup(1);
		fieldsignmes(MES_MASK | (NPC_NAME_START + NPC_REKS));
		stdmotionread(0);
		stdmotionreadsync();
		setwalkspeed(getdefaultwalkspeed());
		stdmotionplay(0x1000000);
		return;
	}

	function main(1)
	{
		return;
	}

	float currAngle;
	int recruitChoice;
	int guestId = 6;
	function talk(2)
	{
		sethpmenu(0);
		ucoff();
		settrapshowstatus(0);
		currAngle = getrotangzx();
		sysRaturna(-1);
		if (isturn())
			turnsync();

		setunazukistatus(1);
		setkutipakustatus(1);

		if (ispartymember(guestId))
		{
			setaskselectignore(0, 0);
			askpos(0, 0, 2);
		}
		else
		{
			setaskselectignore(0, 1);
			setaskselectignore(0, 2);
			askpos(0, 0, 1);
		}

		recruitChoice = aaske(0, MES_MASK | (NPC_RECRUIT_START + NPC_REKS));
		mesclose(0);
		messync(0, 1);
		switch (recruitChoice)
		{
			case 0:
				fadeout(10);
				fadesync();
				
				addpartymember(6);

				if ((allData[LICENSE_ARR_START + 6*CHAR_OFFSET +  3] & 0x80) == 0 || reksLP == 0)
				{
					setgambit_slotmax(6, 5);
				}
				allData[LICENSE_ARR_START + 6*CHAR_OFFSET +  3] |= 0x80; //starting license
				allData[LICENSE_ARR_START + 6*CHAR_OFFSET + 11] |= 0x04;
				allData[LICENSE_ARR_START + 6*CHAR_OFFSET + 17] |= 0x02;
				allData[LICENSE_ARR_START + 6*CHAR_OFFSET + 34] |= 0x10;

				for (regI0 = 0; regI0 < 5; regI0++)
				{
					regI1 = allData16[((EQUIP_ARR_START + 6*CHAR_OFFSET)>>1) + regI0];
					if (regI1 != 0x1000 && regI1 != 0xffff)
					{
						if (haveitem(regI1) == 0)
						{
							if (eqQuantities[((regI1 & 0x1ff) << 1) + 1] == 0)
							{
								additem(regI1, 1);
								eqQuantities[((regI1 & 0x1ff) << 1) + 1] = 4;
							}
						}
					}
				}

				partyallread();
				healall(1);
				
				showparty();
				fadein(10);
				fadesync();
				break;
			case 1:
				fadeout(10);
				fadesync();
				
				removepartymember(6);
				partyallread();
				healall(1);
				
				showparty();
				fadein(10);
				fadesync();
				break;
			case 2:
				modifyEquipmentId = guestId;
				sysReqew(0, modifyEquip::displayMessage);
				ucon();
				sysReqew(0, modifyEquip::changeEquipment);
				break;
		}
		setunazukistatus(0);
		setkutipakustatus(0);
		aturn_261(currAngle);
		settrapshowstatus(1);
		ucon();
		sethpmenu(1);
		return;
	}
}
#endif //ADD_REKS

import global u_char allData[353936] = 0;
import global u_short allData16[176968] = 0;
import global int allData32[88484] = 0;
import global u_char vaanData[0x1c8] = CHAR_DATA_START;

#define MES_MODIFY_POSX 960
#define MES_MODIFY_POSY 540
#define MES_MODIFY_DIALOG (MES_MASK | 178)
u_char vaanCopy[0x1c8];
int modifyEquipmentId;
script modifyEquip(6)
{
	function init()
	{
		return;
	}

	function displayMessage()
	{
		if (modifyEquipmentId < 6) return;
		else if (modifyEquipmentId > 15) return;

		setmesmacro(7, 0, 4, modifyEquipmentId);
		mesopenspeed(7, 0);
		ames(7, MES_MODIFY_DIALOG, MES_MODIFY_POSX, MES_MODIFY_POSY, WIN_ALIGN_CENTER);
		messync(7, 1);
		return;
	}

	function changeEquipment()
	{
		if (modifyEquipmentId < 6) return;
		else if (modifyEquipmentId > 15) return;

		regI2 = 0; //vaan's cost to refund
		for (regI0 = 0; regI0 < 46; regI0++)
		{
			for (regI1 = 0; (regI1 < 8) && (regI0*8 + regI1 < 362); regI1++)
			{
				if ((allData[LICENSE_ARR_START + regI0] >> regI1) & 1)
					regI2 += licensesCost[regI0*8 +regI1];
			}
		}
		regI2 -= 46; //cost of starting licenses

		regI3 = 0; //guest's cost to refund
		for (regI0 = 0; regI0 < 46; regI0++)
		{
			for (regI1 = 0; (regI1 < 8) && (regI0*8 + regI1 < 362); regI1++)
			{
				if ((allData[LICENSE_ARR_START + CHAR_OFFSET*modifyEquipmentId + regI0] >> regI1) & 1)
					regI3 += licensesCost[regI0*8 +regI1];
			}
		}
		switch (addGuestIdNumber)
		{
			case 7: //amalia - ashe
				regI3 -= 111; //cost of starting licenses
				break;
			case 9: //basch - basch
			case 12: //vossler
				regI3 -= 96; //cost of starting licenses
				break;
			case 13: //larsa - penelo
				regI3 -= 61; //cost of starting licenses
				break;
			case 14: //reddas - vaan
			case 6: //reks
				regI3 -= 46; //cost of starting licenses
				break;
		}

		regI2 += vaanLP - regI3;
		if (regI2 > 99999) regI2 = 99999;
		if (regI2 < 0) regI2 = 0;

		allData32[(CHAR_LP_START + CHAR_OFFSET*modifyEquipmentId) >> 2] = regI2;

		for (regI0 = 0; regI0 < CHAR_OFFSET; regI0++)
			vaanCopy[regI0] = vaanData[regI0];
		
		for (regI0 = 8; regI0 < CHAR_OFFSET; regI0++)
			vaanData[regI0] = allData[CHAR_DATA_START + CHAR_OFFSET*modifyEquipmentId + regI0];

		//partyallread();
		healall(1);

		openfullscreenmenu_48d(-1,0);

		for (regI0 = 8; regI0 < CHAR_OFFSET; regI0++)
			allData[CHAR_DATA_START + CHAR_OFFSET*modifyEquipmentId + regI0] = vaanData[regI0];
		
		for (regI0 = 0; regI0 < CHAR_OFFSET; regI0++)
			vaanData[regI0] = vaanCopy[regI0];

		//partyallread();
		healall(1);

		return;
	}

	u_char  licensesCost[362]	= {
		 50,  75, 100, 125,   0,   0,   0,   0,	//quickenings and unused
		  0,   0,   0,   0,   0,   0,   0,   0,	//unused
		  0,   0,  20,  30,  35,  50,  50, 100,	//belias-famfrit
		 30,  50, 100,  65,  65, 115, 200,   1,	//zalera-starting
		 
		 15,  25,  35,  50,  55,  60,  70,  50,	//swords1-bloodsword
		 50,  70,  80, 135, 225,  35,  50,  70,	//greatswords-katana3
		 90, 130, 100, 120, 180,  20,  25,  35,	//katana4-spears3
		 60,  40,  70, 240,  20,  30,  35,  40,	//spears4-poles4
		 
		 50,  90,  20,  30,  35,  45,  60,  70,	//poles5-bows6
		130,  25,  40,  60, 115,  30,  50,  60,	//sagittarius-guns3
		 70,  90, 100,  20,  25,  35,  50,  60,	//guns4-axes5
		 65,  85,  15,  20,  35,  45,  60, 220,	//axes6-mina
		 
		 20,  30,  40,  50, 130,  15,  25,  30,	//rods1-staves3
		 40, 115,  30,  40,  60,  65,  50,  40,	//staves4-measures1
		 50,  70,  35,  55,  75,  15,  20,  25,	//measures2-shields3
		 30,  35,  40, 100,  90, 235,  25,  30,	//shields4-heavy2
		 
		 35,  40,  50,  55,  60,  65,  70,  80,	//heavy3-heavy10
		190,  10,  15,  20,  25,  30,  40,  50, //genji-light7
		 60,  70,  75,  80,  90,  10,  15,  20, //light8-mystic3
		 25,  30,  40,  50,  60,  70,  75,  80, //mystic4-mystic11
		 
		 90,   5,  20,  25,  35,  35,  35,  40, //mystic12-acc7
		 45,  30,  40,  45,  60,  60,  70,  70, //acc8-acc15
		 70,  80,  80, 100, 115, 215,  15,  20, //acc16-white2
		 25,  30,  40,  50,  60,  70,  15,  20, //white3-black2
		 
		 25,  30,  40,  50,  60,  70,  20,  30, //black3-time2
		 40,  50,  60,  70,  80,  40,  50,  90, //time3-green3
		 90, 100, 110, 155,  40,  50, 110, 100, //white10-arcane-black11
		120, 165, 100,  30,  30,  30,  30,  65, //black12-time9-warmage-adrenaline
		 
		 65,  70,  70,  70,  30,  90,  25,  45, //Spellbreaker-Focus-Serenity-Last Stand-Spellbound-Brawler-Shield Block2
		 75,  30,  50,  80,  30,  50,  80,  20, //shield block3-channeling-swiftness-remedy1
		 30,  70,  20,  35,  70,  20,  35,  70, //remedy2-potion-ether
		 30,  50,  90,  30,  30,  30,  30,  50, //phoenix-battle
		 
		 30,  30,  30,  30,  50,  20,  30,  40, //magic-hp110
		 50,  60,  15,  20,  25,  30,  35,  40, //hp150-gambit
		 45,  50,  70, 100,  20,  25,  20,  30, //gambit-steal-libra-first aid-poach
		 30,  50,  35,  50,  40,  50,  40,  30, //charge-horology-souleater-traveler-numerology-shear-achilles-gill toss
		 
		 30,  40,  30,  50,  30,  70,  40,  50, //charm-sight-infuse-addle-bonecrusher-shades-stamp-expose
		 40,  40,  50,  80,  70,  80,  90, 100, //revive-1000-wither-telekinesis-hp230-350
		115, 130, 220,  50,  50,  50,  70,  70, //hp390-500-battle
		 70,  70, 100, 100, 100, 100,  50,  50, //battle-magick
		 
		 50,  70,  70,  70,  70, 100, 100, 100, //magick
		100,  80,  90,  80, 100, 150, 100, 200, //magick-swords8-9-karkata-great4-excalipor-katana5-kumbha
		120, 100,  60, 180,  90, 200, 190, 175, //ninja3-vrsabha-pole6-kanya-bow7-dhanusha-mithuna-vrscika
		 80,  65, 100, 105,  65,  90, 110, 110, //daggers6-staves5-mesures4-makara-shields7-heavy11-12-light13
		
		110, 130, 160,  80,  90,  90,  90, 125, //mystic13-acc21-22-white9-black9-time8-black10-time10
		 30,   0 //second board
	};
}

import global u_char	eqQuantities[1024]	= 0x67c8;
int addGuestIdNumber;
int removeGuestIdNumber;
script addGuest(6)
{
	function init()
	{
		return;
	}

	function removeAllGuests()
	{
		for (regI0 = 7; regI0 < 16; regI0++)
			removeguestbattlemember(regI0);
		return;
	}

	function removeGuest()
	{
		if (removeGuestIdNumber < 7) return;
		else if (removeGuestIdNumber > 15) return;

		fadeout(10);
		fadesync();
		hideparty();

		removeguestbattlemember(removeGuestIdNumber);
		
		showparty();
		fadein(10);
		fadesync();
		return;
	}

	function addReplaceGuest()
	{
		if (addGuestIdNumber < 7) addGuestIdNumber = 7;
		else if (addGuestIdNumber > 15) addGuestIdNumber = 15;

		fadeout(10);
		fadesync();
		hideparty();
		for (regI0 = 7; regI0 < 16; regI0++)
			removeguestbattlemember(regI0);
		
		addguestbattlemember(addGuestIdNumber);
		allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET +  3] |= 0x80; //starting license
		switch (addGuestIdNumber)
		{
			case 7: //amalia - ashe
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET +  4] |= 0x02;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 14] |= 0x20;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 15] |= 0x40;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 20] |= 0x04;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 22] |= 0x40;
				break;
			case 9: //basch - basch
			case 12: //vossler
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET +  4] |= 0x02;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 14] |= 0x20;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 15] |= 0x40;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 34] |= 0x20;
				break;
			case 13: //larsa - penelo
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 11] |= 0x04;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 18] |= 0x20;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 22] |= 0x40;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 34] |= 0x40;
				break;
			case 14: //reddas - vaan
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 11] |= 0x04;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 17] |= 0x02;
				allData[LICENSE_ARR_START + addGuestIdNumber*CHAR_OFFSET + 34] |= 0x10;
				break;
		}

		for (regI0 = 0; regI0 < 5; regI0++)
		{
			regI1 = allData16[((EQUIP_ARR_START + addGuestIdNumber*CHAR_OFFSET)>>1) + regI0];
			if (regI1 != 0x1000 && regI1 != 0xffff)
			{
				if (haveitem(regI1) == 0)
				{
					if (eqQuantities[((regI1 & 0x1ff) << 1) + 1] == 0)
					{
						additem(regI1, 1);
						eqQuantities[((regI1 & 0x1ff) << 1) + 1] = 4;
					}
				}
			}
		}
		showparty();
		fadein(10);
		fadesync();
		return;
	}
}
